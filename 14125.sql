drop table if exists memberUser;
drop table if exists memberGroup;
drop table if exists members;
drop table if exists user;
drop table if exists groups;
drop table if exists groupMembers;
drop table if exists accessRights;
drop table if exists msgSourceStore;
drop table if exists msgSinkStore;
drop table if exists msgAttachment;
drop table if exists memberLabels;
drop table if exists memberLabelsGroup;
drop table if exists memberMsgLabels;
drop trigger if exists updateSysUserId;
drop trigger if exists validateEmail;
drop trigger if exists updateSysGroupId;
drop trigger if exists checkSubGroup;

create table members(
    sysMemberId integer primary key AUTOINCREMENT,
    type text check(type in ('user','group'))
);

create table user(
    sysUserId integer primary key AUTOINCREMENT,
    userId text unique,
    userName text,
    dob date,
    email text,
    password text
);

create table memberUser(
    sysMemberId integer,
    sysUserId integer,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade,
    foreign key(sysUserId) references user(sysUserId) on delete cascade on update cascade
);

create trigger updateSysUserId after insert on user
begin
    insert into members (type) values ('user');
    insert into memberUser(sysMemberId,sysUserId) select max(sysMemberId), max(sysUserId) from members, user;
end;

create trigger validateEmail insert on user
begin
    select case
    when new.email not like '%_@__%.__%'
    then RAISE (ABORT, 'Invalid email address')
    end;
end;

create table groups(
    sysGroupId integer primary key AUTOINCREMENT,
    groupId text unique,
    groupName text
);

create table memberGroup(
    sysMemberId integer,
    sysGroupId integer,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade,
    foreign key(sysGroupId) references groups(sysGroupId) on delete cascade on update cascade
);

create trigger updateSysGroupId after insert on groups
begin
 	insert into members (type) values ('group');
    insert into memberGroup(sysMemberId,sysGroupId) select max(sysMemberId), max(sysGroupId) from members, groups;
end;

create table accessRights(
    accessRight text check(accessRight in ('view','edit')),
    accessDescription text
);

create table groupMembers(
    sysGroupId integer,
    sysMemberId integer,
    accessRight text,
    isOwner boolean,
    foreign key(sysGroupId) references groups(sysGroupId) on delete cascade on update cascade,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade,
    foreign key(accessRight) references accessRights(accessRight) on delete cascade on update cascade
);

create trigger checkSubGroup after insert on groupMembers
begin
    select case
	when ((select new.sysGroupId FROM  groupMembers a ,memberGroup b WHERE  new.sysMemberID = a.sysGroupId AND  new.sysGroupId = b.sysMemberID) ISNULL)
	then RAISE(ABORT, 'group cannot become member of its subGroup')
	end;
end;

create table msgSourceStore(
    sysUserId integer,
    msgId integer,
    msgText text,
    msgTimestamp timestamp,
    draftOrSent text check(draftOrSent in ('draft','sent')),
    foreign key(sysUserId) references user(sysUserId) on delete cascade on update cascade
);

create table msgSinkStore(
    sysMemberId integer,
    msgId integer,
    msgText text,
    msgTimestamp timestamp,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade
);

create table msgAttachment(
    msgId integer,
    attachId integer primary key AUTOINCREMENT,
    attachName text,
    attachBlob blob
);

create table memberLabels(
    sysMemberId integer,
    label text,
    labelDescription text,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade
);

create table memberLabelsGroup(
    sysMemberId integer,
    label text,
    parentLabel text,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade,
    foreign key(parentLabel) references memberLabels(label) on delete cascade on update cascade
);

create table memberMsgLabels(
    sysMemberId integer,
    msgId integer,
    label text,
    foreign key(sysMemberId) references members(sysMemberId) on delete cascade on update cascade,
    foreign key(label) references memberLabels(label) on delete cascade on update cascade
);
