import sys
import os
import time
import hashlib
import thread


from watcher.component.FileWatchManager import WatchManager
from watcher.lib.daemon import Daemon
from watcher.models.context import Context


# Some required setup
currentDirectory = os.getcwd()
configDirectory = os.path.join(currentDirectory, ".filewatcher")
if not os.path.exists(configDirectory):
        os.makedirs(configDirectory)
pidFile = os.path.join(currentDirectory, ".filewatcher/pidfile.pid")
abspath = os.path.dirname(os.path.abspath(__file__));

if __name__ == "__main__":
        print abspath
        context = Context(sys.argv, currentDirectory, abspath, configDirectory)
        daemon = WatchManager(pidFile, context)
        if len(sys.argv) >= 2:
                if 'start' == sys.argv[1]:
                      daemon.start()
                elif 'stop' == sys.argv[1]:
                        daemon.stop()
                elif 'restart' == sys.argv[1]:
                        daemon.restart()
                else:
                        print "Unknown command"
                        sys.exit(2)
        else:
                print "usage: %s start|stop|restart|init" % sys.argv[0]
                sys.exit(2)
