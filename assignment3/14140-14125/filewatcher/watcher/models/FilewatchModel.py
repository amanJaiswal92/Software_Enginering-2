class WatchModel(object):

    def __init__(self, filename, filecontent, priority, compressed = False):
        self._filename = filename
        self._filecontent = filecontent;
        self._priority = priority
        self.compressed = compressed
    
    @property
    def filename(self):
        return self._filename;

    @filename.setter
    def filename(self, filename):
        self._filename = filename

    @property
    def filecontent(self):
        return self._filecontent;

    @filecontent.setter
    def filecontent(self, filecontent):
        self._filecontent = filecontent


    @property
    def priority(self):
        return self._priority;

    @priority.setter
    def priority(self, priority):
        self._priority = priority

    @property
    def compressed(self):
        return self._compressed;

    @compressed.setter
    def compressed(self, compressed):
        self._compressed = compressed
