import sys
import os
import time
import glob

from watcher.lib.daemon import Daemon
from watcher.lib.sqlArbiter import SQLArbiterContext as DBContext

from watcher.service.filewatcher import Watchman

from watcher.repository.FilewatcherRepository import FilewatchRepository as Repository

from watcher.config.configReader import ConfigReader


def initializeWatchman(config, context):
    sqlfile = os.path.join(context.abspath, config.sql.sqlfile)
    with open(sqlfile, 'r') as f:
        sqlstmts = f.read()
    dbname = os.path.join(context.wd, config.sql.dbname);
    dbcontext = DBContext(dbname, sqlstmts)
    repo = Repository(dbcontext)
    return Watchman(repo);

class WatchManager(Daemon):        
    def run(self, context):
        os.chdir(context.directory)
        if len(context.argv) <= 2:
            raise Exception("No Config File provided");
        
        fconfig = context.argv[2];
        config = ConfigReader(fconfig).config;
        watchman = initializeWatchman(config, context);

        while True:
            watchman.watch();
            time.sleep(config.manager.sleepDuration)
        
