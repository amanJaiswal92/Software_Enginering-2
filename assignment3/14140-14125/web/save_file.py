#!/usr/bin/python
import cgi, os
import cgitb
cgitb.enable()
import sys
import json
from cgi import parse_header

cwd = os.path.dirname(os.path.abspath(__file__))
form = cgi.FieldStorage()

fileitem = form['filename']
priority = form.getvalue('priority') | 100
response = {
    "status" : "failed",
    "code" : 500
}

uploadDir = os.path.join(cwd, "upload");
if not os.path.exists(uploadDir):
        os.makedirs(uploadDir)
        
if fileitem.filename:
        fn = os.path.basename(fileitem.filename.replace("\\", "/" ))
        filenme = os.path.join(uploadDir, fn)
        filenmp = os.path.join(uploadDir, "." + fn) + "." + priority
        open(filenme, 'wb').write(fileitem.file.read())
        open(filenmp, 'wb').write("")
        response["status"] = "success"
        response["code"] = 200

print('Content-Type: application/json\n\n');
print json.dumps(response);

 
