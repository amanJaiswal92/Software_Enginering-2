#!/usr/bin/python
import cgi, os
import cgitb
cgitb.enable()
import sys
import json
import glob
from cgi import parse_header

cwd = os.path.dirname(os.path.abspath(__file__))
form = cgi.FieldStorage()

fileitem = form.getvalue('filename')
priority = form.getvalue('priority')

response = {
    "status" : "failed",
    "code" : 500
}

uploadDir = os.path.join(cwd, "upload");
if not os.path.exists(uploadDir):
        os.makedirs(uploadDir)

os.chdir(uploadDir);
for iFile in glob.glob("." + fileitem+ ".*[0-9]*"):
        os.remove(iFile);
        
filenmp = os.path.join(uploadDir, "." + fileitem) + "." + priority
open(filenmp, 'wb').write("")
response["status"] = "success"
response["code"] = 200

print('Content-Type: application/json\n\n');
print json.dumps(response);

 
