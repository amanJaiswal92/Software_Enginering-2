#!/usr/bin/python

import cgi
import json
import cgitb
cgitb.enable()
import os
import glob

cwd = os.path.dirname(os.path.abspath(__file__))


def getRespStructure(filename, priority, status):
    return ({
        "filename": filename,
        "priority": priority,
        "status"  : status
    })

def getFileName(fileName, extension):
    extension = "." + extension;
    fName = (fileName[1:]).split(extension, 1)[0]
    fName = fName + extension;
    return fName

def getStatus(fileName):
    last = fileName.split('.')
    if len(last) > 4:
        return str(last[len(last) - 1])
    return "waiting"

def getFilePriority(fileName):
    last = fileName.split('.')
    if len(last) == 4:
        return int(last[len(last) - 1])

    priority = int(last[len(last) - 2])
    return priority

# fileitem = form['filename']
# priority = form.getvalue('priority') | 100
responses = []
uploadDir = os.path.join(cwd, "upload");
if os.path.exists(uploadDir):
    os.chdir(uploadDir);
    for iFile in glob.glob(".*.json.*[0-9]*"):
        filename = getFileName(iFile, "json");
        status = getStatus(iFile)
        priority = getFilePriority(iFile)
        response = getRespStructure(filename, priority, status)
        responses.append(response)


print('Content-Type: application/json\n\n');
print json.dumps(responses);
