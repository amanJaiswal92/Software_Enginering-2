# Assignment 2 14140-14125

This application currently contains 2 backend modules
  - Filewatcher
    - Compressor

Yet to be restructured module:
  - Validator

This application is an assignment submission for PUCSD Subject SE-2(CS602) Which achieves following goals

- A Python CGI Based Web Backend
- A Python based filewatcher and compressor to watch compress validate json files

### To run

Following Configs are Required

* [/etc/apache2/apache.conf] - To execute CGI Scripts
* [/etc/apache2/sites-available/000-default.conf] - To change root directory for apache server


### Usage

This app requires filewatcher and compressor service to watch the web/upload folder

### Web Path

* localhost[:port]/index : Home Page
* localhost[:port]/changePriority : to Chagne priority of uploaded files