from watcher.models.FilewatchModel import WatchModel


def SQL2WatchModel(record):
    return WatchModel(record[1], record[0], record[2], record[3]);

def SQLList2WatchModel(records):
    return map(SQL2WatchModel, records);
