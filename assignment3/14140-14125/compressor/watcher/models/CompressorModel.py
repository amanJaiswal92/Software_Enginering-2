class WatchModel(object):

    def __init__(self, filename, filecontent, checksum, priority, valid = "NULL"):
        self._filename = filename
        self._filecontent = filecontent
        self._checksum = checksum
        self._priority = priority
        self._valid = valid
    
    @property
    def filename(self):
        return self._filename;

    @filename.setter
    def filename(self, filename):
        self._filename = filename

    @property
    def filecontent(self):
        return self._filecontent;

    @filecontent.setter
    def filecontent(self, filecontent):
        self._filecontent = filecontent


    @property
    def priority(self):
        return self._priority;

    @priority.setter
    def priority(self, priority):
        self._priority = priority

    @property
    def valid(self):
        return self._valid;

    @valid.setter
    def valid(self, valid):
        self._valid = valid

    @property
    def checksum(self):
        return self._checksum;

    @checksum.setter
    def checksum(self, checksum):
        self._checksum = checksum
