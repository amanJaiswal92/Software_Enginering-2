import sys

from watcher.config.FileWatcherConfig import fwconfig;

from watcher.lib.sqlArbiter import SQLArbiter

fconfig = fwconfig["sql"]


# To-do: Call this from filewatcher module

# To-do: move this to commmon utility
def listToQuotedStrings(lst):
    return ','.join(['\'{}\''.format(value) for value in lst])



'''
- FilewatcherRepository extends SQLArbiter
- UseCase: a plug between python application and SQL
  for filewatcher
- Usage: Filewatcher(<dbname|dbpath>)
'''

class FilewatchRepository(SQLArbiter):

    # Called for setting up database using sql file in specified DB.
    def setupDB(self, sqlstmts):
        super(FilewatchRepository, self).executeSql(sqlstmt);

    # Called for creating table in the Specified DB.
    def createTable(self):
        query_sql = fconfig["createtable"]
        super(FilewatchRepository, self).executeSql(query_sql);

    # Called for inserting/updating data in the table
    def insertValues(self, watchModel, tableName):
        attrs = vars(watchModel);
        column_values = [x[1] for x in attrs.items()]
        column_values = listToQuotedStrings(column_values)
        query_sql = fconfig["insertquery"].format(tablename=tableName, values=column_values);
        super(FilewatchRepository, self).executeSql(query_sql);

    def fetchuncompressed(self, tableName):
        print tableName
        query_sql = fconfig["selectquery"].format(tablename="temptable", compressed="False");
        response = super(FilewatchRepository, self).executeSql(query_sql);
        return response
