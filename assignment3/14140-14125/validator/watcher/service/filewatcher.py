# Imports start

# To-Do: optimize imports
import os
import sys
import logging
import glob
import time
import traceback
import json
import sqlite3
import base64
import hashlib

#from FilewatchModel import WatchModel;
from watcher.models.CompressorModel import WatchModel

# Imports end
def isValid(data):#not completed
    return True

def decompressData(data):
    return base64.b64decode(data)

def getChecksum(data):
    hash_md5 = hashlib.md5()
    hash_md5.update(data)
    return hash_md5.hexdigest()


def getFileName(fileName, extension):
    extension = "." + extension;
    fName = (fileName[1:]).split(extension, 1)[0]
    fName = fName + extension;
    return fName

def getFilePriority(fileName):
    last = fileName.split('.')
    priority = int(last[len(last) - 1])
    return priority
    


def setupDirectory(path):
    if not os.path.exists(path):
        os.makedirs(path);


'''
 - To-Do#1: Move this to Utils
 - To-Do#2: Change logic to handle large files in blocks
'''
def readFile(fileName):
    fileBytes = open(fileName, "rb")
    data = fileBytes.read();
    fileBytes.close();
    return data;

'''
 - To-Do: Move this to utils
'''
def markFile(filename, suffix):
    os.rename(filename, filename+suffix)

class Watchman(object):
    def __init__(self, repo):
        self.repo = repo
        
    def saveRecord(self, dataModel):
        self.repo.insertValues(dataModel, "temptable");

    def dbCall(self, dataModel):
        try:
            self.saveRecord(dataModel)
            return True
        except Exception as e:
            print (e)
            return False

            
    def watch(self, records):
        for record in records.affectedrows:
            compressedContent = record[0]
            content = decompressData(record[0])
            checkSum = record[1]
            fileName = record[2]
            priority = record[3]
            dataModel = WatchModel(fileName, compressedContent, checkSum, priority, isValid(content))
            return self.dbCall(dataModel)
