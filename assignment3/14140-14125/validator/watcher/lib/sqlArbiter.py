import sqlite3

class SQLArbiterResponse(object):
    def __init__(self, affectedrows, rowcount):
        self._affectedrows = affectedrows
        self._rowcount = rowcount

    @property
    def affectedrows(self):
        return self._affectedrows;

    @affectedrows.setter
    def affectedrows(self, affectedrows):
        return self._affectedrows;

    @property
    def rowcount(self):
        return self._rowcount;

    @rowcount.setter
    def rowcount(self, rowcount):
        self._rowcount = rowcount;


class SQLArbiterContext(object):
    def __init__(self, dbname, sqlstmts):
        self._dbname = dbname;
        self._sqlstmts = sqlstmts

    @property
    def dbname(self):
        return self._dbname

    @dbname.setter
    def dbname(self, dbname):
        self._dbname = dbname

    @property
    def sqlstmts(self):
        return self._sqlstmts

    @sqlstmts.setter
    def sqlstmts(self, sqlstmts):
        return self._sqlstmts

'''
 SQLArbiter is the mediator between SQL and our
 python module. All classes dealing with different
 DBs should extend this to simplify the stuff.
'''
class SQLArbiter(object):
    connection = None;

    ''' 
     Constructor must create a connection for
     provided db file and execute the sqlfile
     for base setup of db provided in the sql
     file.
    '''
    def __init__(self, dbcontext):
        try:
            connection = sqlite3.connect(dbcontext.dbname);
            self.connection = connection
        except Exception as e:
            raise Exception(e);
        sqlstmts = dbcontext.sqlstmts.split(';')
        for sqlstmt in sqlstmts:
            self.executeSql(sqlstmt)

        
    '''
     SQL Query executor for private connection
     an instance is a connection between db and
     python modules
    '''
    def executeSql(self, query_sql):
        connection = self.connection;
        response = None;
        # Raise exception if connection is not yet initialized
        if connection == None:
            raise Exception("Connection used before creation");
        
        try:
            cursor = connection.cursor();
            cursor.execute(query_sql);
            connection.commit();
            records = cursor.fetchall();
            rowcount = cursor.rowcount;
            response = SQLArbiterResponse(records, rowcount);
        except Exception as e:
            raise Exception(e)

        return response;
