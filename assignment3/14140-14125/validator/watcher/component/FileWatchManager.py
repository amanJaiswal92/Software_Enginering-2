import sys
import os
import time
import glob

from watcher.lib.daemon import Daemon
from watcher.lib.sqlArbiter import SQLArbiterContext as DBContext

from watcher.service.filewatcher import Watchman

from watcher.repository.FilewatcherRepository import FilewatchRepository as Repository

from watcher.config.configReader import ConfigReader

from watcher.models.FilewatchModel import WatchModel

from watcher.utils.utils import SQLList2WatchModel

def initializeWatchman(config, context):
    sqlfile = os.path.join(context.abspath, config.sql.sqlfile)
    with open(sqlfile, 'r') as f:
        sqlstmts = f.read()
    dbname = os.path.join(context.wd, config.sql.dbname);
    dbcontext = DBContext(dbname, sqlstmts)
    repo = Repository(dbcontext)
    return repo;

def connectDB(dbname):
    dbcontext = DBContext(dbname, "")
    repo = Repository(dbcontext)
    return repo

def readDB(repo, table):
    records = repo.fetchuncompressed(table);
    return records



class WatchManager(Daemon):
    def markCompressed(self, records):
        conn = self.conn;
        for record in records:
            record.compressed = True;
            conn.insertValues(record, "temptable")
        
        
    def run(self, context):
        os.chdir(context.directory)
        if len(context.argv) <= 2:
            raise Exception("No Config File provided");
        
        fconfig = context.argv[2];
        config = ConfigReader(fconfig).config;
        compressedDB = initializeWatchman(config, context);
        readRepo = connectDB(config.data.dblocation)
        self.conn = readRepo
        watchman = Watchman(compressedDB);

        while True:
            records = readDB(readRepo, "temptable");
            print "Records ", records.affectedrows;
            models = SQLList2WatchModel(records.affectedrows)
            response = watchman.watch(records);
            if response:
                self.markCompressed(models);
            time.sleep(config.manager.sleepDuration)
