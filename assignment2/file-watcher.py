import os
import sys
import argparse
import logging.config
import os
import json

logger = logging.getLogger(__name__)

def setupLogger(default_path='logging.json',default_level=logging.INFO,env_key='LOG_CFG'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
        if os.path.exists(path):
            with open(path, 'r') as f:
                config = json.load(f)
                logging.config.dictConfig(config)
        else:
            logging.basicConfig(level=default_level)

def validationOfPath(path):
    try:
        if not os.path.isdir(path):
            raise ValueError("Path is not Valid")
    except ValueError as err:
        logger.error(err)

def readFile(fileName):
    print(fileName)
    try:
        with open(fileName) as data_file:
            data = json.load(data_file)
            logger.debug(data)
    except (IOError,OSError) as e:
        logger.error(e)


def main(argv):
    setupLogger(default_path='logging.json',default_level=logging.INFO,env_key='LOG_CFG')
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('-i','--input',help="input directory path",required=True)
    args = parser.parse_args()
    logger.debug(args)
    try:
        validationOfPath(args.input)
        #return 0
    except:
        print("eeee")
        #return 1
    readFile('/home/aman/se2-2017/assignment2/sample-json-record-1.json')


if __name__=="__main__":
    main(sys.argv)
