import sqlite3

connection = None
try:
    connection = sqlite3.connect('se_2.db')
    cursor = connection.cursor()
except sqlite3.DatabaseError as e:
    print('Error %s' %e)
    sys.exit(1)
