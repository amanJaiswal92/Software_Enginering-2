import zlib, base64

def compress(fileName):
    try:
        with open(fileName) as data_file:
            data = data_file.read()
            data = base64.b64encode(zlib.compress(bytes(data,'UTF-8'),9))
            try:
                with open("fileNameMd5","wb") as outfile:
                    outfile.write(data)
            except (IOError,OSError) as e:
                print(e)
    except (IOError,OSError) as e:
        print(e)

def decompress(fileName):
    try:
        with open(fileName) as data_file:
            data = data_file.read()
            data = zlib.decompress(base64.b64decode(data))
            try:
                with open("output.json","wb") as outfile:
                    outfile.write(data)
            except (IOError,OSError) as e:
                print(e)
    except(IOError,OSError) as e:
        print(e)

def main():
    fileName = '/home/aman/se2-2017/assignment2/sample-json-record-1.json'
    fileName2 =  '/home/aman/se2-2017/assignment2/fileNameMd5'
    compress(fileName)
    decompress(fileName2)
if __name__=="__main__":
    main()
