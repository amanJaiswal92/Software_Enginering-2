def readFile(fileName):
    print(fileName)
    try:
        with open(fileName) as data_file:
            data = json.load(data_file)
            logger.debug(data)
    except (IOError,OSError) as e:
       logger.error(e)


readFile()
