#!/usr/bin/python

ifconfig
ping www.google.com
route -n
sudo ovs-vsctl show
sudo ovs-vsctl add-br br-int #to add bridge 
sudo ovs-vsctl add-port br-int wlan0
sudo ifconfig wlan0 0 #to remove ip of wlan0 from ifconfig and route table
sudo ifconfig br-int 10.30.13.159 netmask 255.255.255.0 #to add wlan0 ip to br-int
sudo route add default gw 192.168.0.1 br-int #to add default entry
sudo ovs-vsctl del-br br-int #to delete bridge
