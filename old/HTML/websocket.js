function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    var mvalue;
    if (parts.length == 2) {
	mvalue = parts.pop().split(";").shift();
	mvalue = mvalue.replace(/['"]+/g, '');
    }
    return mvalue;
}


function ChatSocket(context) {
    context = context || {};
    var userCommands = context.commands || {};

    this.socket = null;

    var defaultCommands = {
	"setCookie": setCookie,
	"reload": reload,
	"showMessage": showMessage
    }
    
    var userEvents = context.events || {};
    var defaultEvents = {
	"onopen": defaultopen,
	"onclose": defaultclose,
	"onmessage": defaultonmessage
    }
    
    var command = Object.assign({}, defaultCommands, userCommands);
    var events = Object.assign({}, defaultEvents, userEvents);

    var types = {
	"command": command
    }

    function setCookie(args) {
	var name = args.key;
	var value = args.value;
	name && value && (document.cookie = name + "=" + value + "; path=/");
    }

    function reload(args) {
	location.reload();
    }

    function showMessage(args) {
	showToast(args.message , args.type);
    }

    function defaultopen(event) {
	console.log("Connection Established");
    }

    function defaultclose(event) {
	console.log("Connection Closed");
    }

    function defaultonmessage(event) {
	var msg = JSON.parse(event.data);
	var category = types[msg.message_type];
	var command = msg.response.command_name;
	category && category.hasOwnProperty(command) && category[command](msg.response.args);
    }

    this.sendMessage = function(message) {
	console.log("Sending Message ", message);
	if (!this.socket) {
	    console.error("No Socket connected yet");
	    return;
	}
	var socket = this.socket;
	socket.send(message);
    }
    
    this.connect = function(url) {
	cid = getCookie("CID") || 0;
	var sock = this.socket || new WebSocket(url, cid);
	sock.onopen = events.onopen;
	sock.onclose = events.onclose;
	sock.onmessage = events.onmessage;
	this.socket = sock;
	return this.socket;
    }
}
