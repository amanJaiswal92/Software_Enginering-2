function loginRequest(oFormElement) {
    var fd = new FormData(oFormElement);
    return JSON.stringify({
	"login": {
	    "emailid": fd.get("_emailid"),
	    "password": fd.get("_password")
	}
    });
}


function login(oFormElement) {
    message = loginRequest(oFormElement);
    sendMessage(message);
    return false;
}
