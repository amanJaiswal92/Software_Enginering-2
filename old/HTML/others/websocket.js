var socket = null;

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    var mvalue;
    if (parts.length == 2) {
	mvalue = parts.pop().split(";").shift();
	mvalue = mvalue.replace(/['"]+/g, '');
    }

    return mvalue;
}

function showMessage(args) {
    showToast(args.message , args.type);
}

function setCookie(args) {
    var name = args.key;
    var value = args.value;
    name && value && (document.cookie = name + "=" + value + "; path=/");
}

function deleteCookie(args) {
    setCookie(args.key, args.value);
}

function reload(args) {
    location.reload();
}

var types = {
    "command": {
	"setCookie": setCookie,
	"reload": reload,
	"showMessage": showMessage
    }
}

function sendMessage(message) {
    console.log("Sending Message ", message);
    var msocket = socket || connectToServer();
    socket.send(message);
}

function connectToServer() {
    cid = getCookie("CID") || 0
    socket = socket || new WebSocket("ws://localhost:9999/", cid);
    socket.onopen = function(event) {
	console.log("Connection established");
    }

    socket.onclose = function(event) {
	console.log("Connection Closed");
    }
    
    socket.onmessage = function(e){
	var msg = JSON.parse(e.data);
	var category = types[msg.message_type];
	var command = msg.response.command_name;
	category.hasOwnProperty(command) && category[command](msg.response.args);
    }
    return socket;

}

window.onload = connectToServer();

