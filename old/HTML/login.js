var chatSocket = new ChatSocket();
function loginRequest(oFormElement) {
    var fd = new FormData(oFormElement);
    return JSON.stringify({
	"login": {
	    "emailid": fd.get("_emailid"),
	    "password": fd.get("_password")
	}
    });
}

function login(oFormElement) {
    message = loginRequest(oFormElement);
    chatSocket.sendMessage(message);
    return false;
}

function connect() {
    var url = "ws://localhost:9999";
    chatSocket.connect(url);
}
window.onload = connect;
