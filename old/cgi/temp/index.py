#! /usr/bin/env python2
import cgi
import cgitb
import time
import os
import json
import sys
import project.util as util
from urlparse import urljoin
from project.Config import ConfigReader

cgitb.enable()
config_file = "project/file.json"

def main(args):
    config = ConfigReader(config_file).config
    registerpage = open(config.template, 'r')
    html = registerpage.read();
    netloc = config.authServer.url
    port = config.authServer.port
    scheme = config.authServer.scheme
    path = config.authServer.registration
    registration = util.joinurl(netloc, path, port, scheme)
    html = html.format(registration=registration)
    print config.resStructure.format(status=200, contype=config.hostServer.responseHeaders.contentType, body=html)

if __name__ == "__main__":
    main(sys.argv)
