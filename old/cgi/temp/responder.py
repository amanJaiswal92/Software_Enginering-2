#! /usr/bin/env python2
import cgi
import json
import uuid
import sys
import traceback
import cgitb
import Cookie
import os
from project.repository.Repository import Repository


from project.models.Reader import Message
from project.models.Reader import ResponseMsgs
from project.models.Reader import Responses
from project.models.Reader import Channel
from project.models.Reader import Communicator
from project.models.Reader import ResponseObj
from project.models.Model import Model

import project.utils.ResponseWrapper as wrapper

from project.Config import ConfigReader
from requestobj import Request
from response import Response

config_file = "project/file.json"
config = ConfigReader(config_file).config
cgitb.enable()

repo = Repository(config.dbServer);

def fetchMessages(msgid):
    message = Message(msgid)
    sqlresp = repo.fetchRecords(message);
    message = [row for row in sqlresp.affectedrows][0]
    return Message(**message)

def fetchCommunications(communicationid):
    communication = Communicator(communicationid)
    sqlresp = repo.fetchRecords(communication)
    communication = [row for row in sqlresp.affectedrows][0]
    communication = Communicator(**communication);
    sender = fetchChannel(Channel(None, communication.sender)).emailid;
    receiver = fetchChannel(Channel(None, communication.receiver)).emailid;
    communication.sendername = sender;
    communication.receivername = receiver;
    msgid = communication.msgid;
    message = fetchMessages(msgid);
    return wrapper.communicatorRespObj(communication, message)


def fetchResponseMsgs(responseid):
    responseMsgs = ResponseMsgs(responseid);
    sqlresp = repo.fetchRecords(responseMsgs);
    resps = []
    for x in sqlresp.affectedrows:
        resps.append(ResponseMsgs(**x))
    communications = map(lambda x: fetchCommunications(x.commid), resps);
    return communications

def getCommunications(rspns):
    communications = []
    if rspns.id:
        communications = fetchResponseMsgs(rspns.id)
    return communications

def fetchResponses(response):
    sqlresp = repo.fetchRecords(response)
    cursor = sqlresp.affectedrows
    rspns = cursor.fetchone();
    rspns = Responses(**rspns) if not rspns == None else response;
    communications = getCommunications(rspns)#map(getCommunications, rspns);
    return ResponseObj(rspns.id, response.requestid, communications)


def fetchChannel(channel):
    sqlresp = repo.fetchRecords(channel)
    cursor = sqlresp.affectedrows
    rspns = cursor.fetchone();
    rspns = Channel(**rspns) if not rspns == None else channel;
    return rspns;

    
def dumpobj(respobj):
    if isinstance(respobj, list) and not isinstance(respobj, str):
        return map(dumpobj, respobj)
    dic = respobj.__dict__
    for ob in dic:
        if isinstance(dic[ob], Model):
            dic[ob] = dumpobj(dic[ob])
        if isinstance(dic[ob], list) and not isinstance(dic[ob], str):
            dic[ob] = map(dumpobj, dic[ob])
    return json.loads(json.dumps(dic))
        

def main():
    cookie=Cookie.SimpleCookie()
    requestid = None;
    if 'HTTP_COOKIE' in os.environ:
        cookie_string=os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
        requestid = cookie["CID"].value if "CID" in cookie else None; 
    # requestid = None;
    if requestid == None:
        request = json.load(sys.stdin);
        requestid = request["_channel"] if "_channel" in request else None
        if requestid == None:
        # requestid = "login@backend.com"
            requestid = request["_sender"]
            requestid = fetchChannel(Channel(requestid)).id;

    status = 200

    response = json.dumps(dumpobj(ResponseObj(None, None, [])));
    if requestid == None:
        status = 400
    try :
        if status == 200:
            response = fetchResponses(Responses(requestid))
            response = json.dumps(dumpobj(response))
    except Exception as e:
        status = 500
        response = e
    print config.resStructure.format(status=status, contype=config.requestResolver.responseHeaders.contentType, body=response)


if __name__ == "__main__":
    main();
