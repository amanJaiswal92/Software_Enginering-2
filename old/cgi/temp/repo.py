import sys
import json

# from filewatcher.watcher.config.FileWatcherConfig import fwconfig;
#from watcher.models.FilewatchModel import WatchModel;
from sqlArbiter import SQLArbiter
from sqlArbiter import SQLArbiterContext


# To-do: Call this from filewatcher module

# To-do: move this to commmon utility
def listToQuotedStrings(lst, delim):
    return ','.join([(delim + '{}' + delim).format(value) for value in lst])



'''
- FilewatcherRepository extends SQLArbiter
- UseCase: a plug between python application and SQL
  for filewatcher
- Usage: FilewatchRepository(<dbname|dbpath>)
'''

class Repository(SQLArbiter):

    # Called for setting up database using sql file in specified DB.
    def setupDB(self, sqlstmts):
        super(Repository, self).executeSql(sqlstmts);
        
    # Called for inserting data in the table
    def insertValues(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel)
        tableName = watchModel.__class__.__name__
        cols = listToQuotedStrings(attrs.keys(), '`')
        qmarks = ", ".join(["%s"] * len(attrs))
        query_sql = config.insertquery.format(tablename=tableName,columnname=cols, values=qmarks)
        return super(Repository, self).executeSql(query_sql, attrs.values())

    # Called for updating data in the table
    def replaceValues(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel)
        tableName = watchModel.__class__.__name__
        cols = listToQuotedStrings(attrs.keys(), '`')
        qmarks = ", ".join(["%s"] * len(attrs))
        query_sql = config.replacequery.format(tablename=tableName,columnname=cols, values=qmarks)
        return super(Repository, self).executeSql(query_sql, attrs.values())
 
    # Called for searching data
    def selectValues(self, watchModel, tableName = None):
        config = self.dbcontext.config
        tableName = tableName or watchModel.__class__.__name__
        attrs = vars(watchModel)
        attrs = {key: value for key, value in attrs.iteritems() if not value == None}
        vals = [attrs[key] for key in attrs]
        conditions = " and ".join(["=".join(['`{}`'.format(key), '{}'.format("%s")]) for key, val in attrs.items()])
        if conditions:
            conditions = "WHERE " + conditions
        # return conditions
        query_sql = config.selectquery.format(tablename=tableName, conditions=conditions)
        return super(Repository, self).executeSql(query_sql, vals)

    
        
