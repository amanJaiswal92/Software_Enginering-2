#! /usr/bin/env python2
import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(("", 9999))
sock.listen(5)

handshake = '\
HTTP/1.1 101 Web Socket Protocol Handshake\r\n\
Upgrade: WebSocket\r\n\
Connection: Upgrade\r\n\
WebSocket-Origin: http://localhost:8888\r\n\
Sec-WebSocket-Key: {0}\r\n\
Sec-WebSocket-Accept: {1}\r\n\
WebSocket-Location: ws://localhost:9999/\r\n\r\n\
'
handshaken = False

print "TCPServer Waiting for client on port 9999"

import sys
import hashlib, base64


h = hashlib.sha1("dGhlIHNhbXBsZSBub25jZQ==258EAFA5-E914-47DA-95CA-C5AB0DC85B11")
 # hexadecimal string representation of the digest
mkey = base64.b64encode(h.digest())
accept = h.digest()

data = ''
header = ''

client, address = sock.accept()
while True:
    if handshaken == False:
        header += client.recv(16)
        if header.find('\r\n\r\n') != -1:
            data = header.split('\r\n\r\n', 1)[1]
            handshaken = True
            handshake = str(handshake).format(mkey, accept)
            print handshake
            client.send(handshake)
    else:
            tmp = client.recv(128)
            data += tmp;
            validated = []
            msgs = data.split('\xff')
            data = msgs.pop()
            for msg in msgs:
                if msg[0] == '\x00':
                    print msg[0]
