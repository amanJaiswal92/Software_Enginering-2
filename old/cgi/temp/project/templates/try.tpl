<html>
  <head>
    <script src="/toastr.js"></script>
    <script src="/websocket.js"> </script>
    <script src="/messenger.js"></script>
    <title>Messenger</title>
    <link rel="stylesheet" type="text/css" href="/css/toast.css">
    <link rel="stylesheet" type="text/css" href="/css/load.css">
    <link rel="stylesheet" type="text/css" href="/css/messaging.css">
  </head>
  <body bgcolor="white">
    <div class="background" id="loader">
      <p class="loader">Loading</p>
    </div>
 
    <div id="app-container" style="display: none">
      <input type="text" id="receiver"/>
      <input type="button" onClick="setReceiverName()" value="setReceiver"/>
      <!-- <input type="button" onClick="logout()" value="Logout" /> -->
      <div class="mainc">
    	<div class="contacts" id="messageWindow">
    	</div>
    	<div class="contact-list" id="contactlist">
    	</div>
    	<div>
    	  <input type="text" id="message" style="width: 70%;" onKeyDown="handleInputBox(event)"/>
    	  <br />
    	  <input type="button" onClick="sendInputBox()" value="Send"/>
    	</div>
      </div>
    </div>
    
  <body>
</html>

<!-- <html> -->
<!--   <head> -->
<!--     <link rel="stylesheet" type="text/css" href="/css/messaging.css"> -->
<!--   </head> -->
<!--   <body bgcolor="white"> -->
<!--     <div class="main" style="color:"> -->
<!--       HelloWorld -->
<!--     </div> -->
<!--     <div class="talk-bubble tri-right btm-left"> -->
<!--       <div class="talktext"> -->
<!-- 	<p>And now using .round we can smooth the sides down. Also uses .btm-left to show a triangle at the bottom flush to the left.</p> -->
<!--       </div> -->
<!--     </div> -->
<!--     <div class="talk-bubble tri-right btm-right"> -->
<!--       <div class="talktext"> -->
<!-- 	<p>And now using .round we can smooth the sides down. Also uses .btm-left to show a triangle at the bottom flush to the left.</p> -->
<!--       </div> -->
<!--     </div> -->
<!--     <div class="talk-bubble tri-right btm-right"> -->
<!--       <div class="talktext"> -->
<!-- 	<p>And now using .round we can smooth the sides down. Also uses .btm-left to show a triangle at the bottom flush to the left.</p> -->
<!--       </div> -->
<!--     </div> -->
<!--     <div class="talk-bubble tri-right btm-left"> -->
<!--       <div class="talktext"> -->
<!-- 	<p>And now using .round we can smooth the sides down. Also uses .btm-left to show a triangle at the bottom flush to the left.</p> -->
<!--       </div> -->
<!--     </div> -->
<!--   <body> -->
<!-- </html> -->
