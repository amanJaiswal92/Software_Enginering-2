<!DOCTYPE html>
<html>
  <head>
    <script src="/toastr.js"></script>
    <script src="/websocket.js"></script>
    <script src="/login.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/toast.css">
    <link rel="stylesheet" type="text/css" href="/css/form.css">
  </head>
  <body>
    <div class='login' id="major">
      <h2>Sign in</h2>
      <form onsubmit="return login(this)">
	<input name='_emailid' placeholder='Email' type='text'/>
	<input id='pw' name='_password' placeholder='Password' type='password'/>
	<div class='remember'>
	  <input checked='' id='remember' name='remember' type='checkbox'/>
	  <label for='remember'></label>Remember me
	</div>
	<input type='submit' value='Sign in'/>
	<a class='forgot' href='#'>Forgot your password?</a> 
      </form>
    </div>
  </body>
</html>
