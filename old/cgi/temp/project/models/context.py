class Context(object):

    def __init__(self, argv, directory, abspath, wd):
        self._argv = argv;
        self._directory = directory;
        self._abspath = abspath;
        self._wd = wd
    
    @property
    def argv(self):
        return self._argv;

    @argv.setter
    def argv(self, argv):
        self._argv = argv

    @property
    def directory(self):
        return self._directory;

    @directory.setter
    def directory(self, directory):
        self._directory = directory

    @property
    def abspath(self):
        return self._abspath;

    @abspath.setter
    def abspath(self, abspath):
        self._abspath = abspath 

    @property
    def wd(self):
        return self._wd;

    @wd.setter
    def wd(self, wd):
        self._wd = wd 
