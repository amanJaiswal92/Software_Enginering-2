from watcher.models.Model import Model

class RequestModel(Model):
    def __init__(self, _requestid, _commid):
        self._requestid = _requestid;
        self._commid   = _commid;

    @property
    def requestid(self):
        return self._requestid;

    @requestid.setter
    def requestid(self, requestid):
        self._requestid = requestid;
 
    @property
    def commid(self):
        return self._commid;

    @commid.setter
    def commid(self, commid):
        self._commid = commid;
