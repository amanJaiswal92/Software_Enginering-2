import yaml



class Config(object):
    def __init__(self, config):
        for key in config:
            if isinstance(config[key], dict):
                config[key] = Config(config[key])
        self.__dict__ = config
         


class ConfigReader(object):
    def __init__(self, fconfig):
        try:
            with open(fconfig, 'r') as f:
                fwconfig = yaml.load(f)
        except Exception as e:
            raise Exception("File Doesn't Exist");
        
        config = Config(fwconfig)
        self._config = config

    @property
    def config(self):
        return self._config
