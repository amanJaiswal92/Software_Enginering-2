#! /usr/bin/env python2

import cgi
import cgitb
import time
import os
import json
import sys
import project.util as util
from urlparse import urljoin
from project.Config import ConfigReader
import requests
import Cookie
import uuid
# cgitb.enable()
config_file = "project/file.json"

messageContent =  "{\"_emailid\": \"%s\",\"_password\": \"randomPassword\",\"_firstname\": \"guest\", \"_lastname\": \"guest\", \"_dob\": \"06-06-1994\" }"

    
guestUser = {
    "_communicator": {
        "_receiver": "channelmanager@backend.com",
        "_sender"  : "homepage@webserver.com",
        "_message" : None
    }
}


deliveryMarker = {
    "_communicator": {
        "_receiver": "postman@backend.com",
        "_sender"  : "homepage@webserver.com"
    }
}

watcherResponse = {
    "_communicator": {
        "_receiver": "Watcher",
        "_sender"  : "homepage@webserver.com"
    }
}


responder = {
    "_sender": "homepage@webserver.com"
}

requestUrl  = 'http://localhost/cgi-bin/requestResolver.py'
responseUrl = 'http://localhost/cgi-bin/responder.py'

def postRequest(url, body):
    r = requests.post(url, data=json.dumps(body))
    return r


def registerUser():
    rv = postRequest(requestUrl, guestUser);
    while True:
        time.sleep(0.1);
        r = postRequest(responseUrl, responder);
        data = json.loads(r.content);
        if (len(data["_communications"])) > 0:
            responseid = data["_responseid"]
            deliveryMarker["_communicator"]["_respid"] = responseid;
            postRequest(requestUrl, deliveryMarker);
            return int(data["_communications"][0]["_message"]["_message"])

    


def main(args):

    cookie=Cookie.SimpleCookie()
    
    if 'HTTP_COOKIE' in os.environ:
        cookie_string=os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
    if not 'CID' in cookie:
        cookie['CID'] = registerUser();

    config = ConfigReader(config_file).config
    page = config.logintemplate;
    dta = {'_channel': cookie['CID'].value}
    r = requests.post("http://localhost/cgi-bin/chkChannel.py", data=json.dumps(dta));
    r = json.loads(r.text);
    if not r["_channel"] == None:
        cookie['userid'] = r["_channel"];
        page = config.messagetemplate;

    registerpage = open(page, 'r')
    html = registerpage.read();
    netloc = config.authServer.url
    port = config.authServer.port
    scheme = config.authServer.scheme
    path = config.authServer.login
    registration = util.joinurl(netloc, path, port, scheme)
    html = html.format(login="requestResolver.py")
    print config.resStructur.format(status=200, cookie=str(cookie), contype=config.hostServer.responseHeaders.contentType, body=html)

if __name__ == "__main__":
    main(sys.argv)
