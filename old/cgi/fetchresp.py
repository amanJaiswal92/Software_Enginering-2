class Model(object):
    def __init__(self):
        self = self
        
class NewResponses(Model):
    def __init__(self, **kwargs):
        self._responseid = kwargs.get("_responseid")
        self._commid     = kwargs.get("_commid")
        self._channelid  = kwargs.get("_channelid")

    @property
    def responseid(self):
        return self._responseid

    @responseid.setter
    def responseid(self, _responseid):
        self._responseid = _responseid

    @property
    def commid(self):
        return self._commid

    @commid.setter
    def commid(self, _commid):
        self._commid = _commid


class Communicator(Model):
    def __init__(self, **kwargs):
        self._id = kwargs.get("_id");
        self._sender = kwargs.get("_sender")
        self._receiver = kwargs.get("_receiver")
        self._msgid = kwargs.get("_msgid")
        self._status = kwargs.get("_status", "Arrived")
        self._senderchannel = kwargs.get("_sender")

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def senderchannel(self):
        return self._senderchannel

    @senderchannel.setter
    def senderchannel(self, senderchannel):
        self._senderchannel = senderchannel

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, _sender):
        self._sender = _sender

    @property
    def receiver(self):
        return self._receiver 

    @receiver.setter
    def receiver(self, _receiver):
        self._receiver = _receiver

    @property
    def msgid(self):
        return self._msgid

    @msgid.setter
    def msgid(self, _msgid):
        self._msgid = _msgid

    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, _status):
        self._status = _status


class MessageReference(Model):
    def __init__(self, **kwargs):
        self._id = kwargs.get('_id')
        self._sender = kwargs.get('_sender')
        self._msgid = kwargs.get('_msgid')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def msgid(self):
        return self._msgid

    @msgid.setter
    def msgid(self, _msgid):
        self._msgid = _msgid

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, _sender):
        self._sender = _sender


class Message(Model):
    def __init__(self, **kwargs):
        self._id = kwargs.get('_id')
        self._message = kwargs.get('_message')
        self._msgtype = kwargs.get('_msgtype')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, _message):
        self._message = _message

    @property
    def msgtype(self):
        return self._msgtype

    @msgtype.setter
    def msgtype(self, _msgtype):
        self._msgtype = _msgtype

        
class GroupDetails(Model):
    def __init__(self, **kwargs):
        self._id = kwargs.get('_id')
        self._admin = kwargs.get('_admin')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def admin(self):
        return self._admin
        
    @admin.setter
    def admin(self, _admin):
        self._admin = _admin

class UserDetails(Model):
    def __init__(self, **kwargs):
        self._id = kwargs.get('_id')
        self._emailid = kwargs.get('_emailid')
        self._usertype = kwargs.get('_usertype')
        self._firstname = kwargs.get('_firstname')
        self._lastname = kwargs.get('_lastname')
        self._dob = kwargs.get('_dob')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def emailid(self):
        return self._emailid

    @emailid.setter
    def emailid(self, _emailid):
        self._emailid = _emailid

    @property
    def usertype(self):
        return self._usertype

    @usertype.setter
    def usertype(self, _usertype):
        self._usertype = _usertype

    @property
    def firstname(self):
        return self._firstname

    @firstname.setter
    def firstname(self, _firstname):
        self._firstname = _firstname

    @property
    def lastname(self):
        return self._lastname

    @lastname.setter
    def lastname(self, _lastname):
        self._lastname = _lastname

    @property
    def dob(self):
        return self._dob

    @dob.setter
    def dob(self, _dob):
        self._dob = _dob
