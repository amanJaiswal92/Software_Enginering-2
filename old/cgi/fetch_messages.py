#! /usr/bin/env python2
import cgi
import cgitb
import time
import os
import json
import sys
import temp.project.util as util
from urlparse import urljoin
import Cookie
from temp.project.Config import ConfigReader
from repo import Repository
from sqlArbiter import SQLArbiterContext as DBContext
from Channel import Channel

from fetchresp import Model
from fetchresp import NewResponses
from fetchresp import Communicator
from fetchresp import MessageReference
from fetchresp import Message
from fetchresp import UserDetails
from fetchresp import GroupDetails

class responsefetcher(object):
    def __init__(self, repo):
        self.repo = repo

    def fetch_records(self, request):
        return self.repo.selectValues(request)

    def fetch_responses(self, cid):
        new_resp = NewResponses(_channelid = cid)
        new_resp_details = self.fetch_records(new_resp)
        responses = []
        for row in new_resp_details.affectedrows:
            response = NewResponses(**(row))
            response = self.fetch_communications(response)
            responses.append(response)
        return responses

    def fetch_channel(self, comm):
        channel = Channel(_id = comm.sender)
        channel_details = self.fetch_records(channel)
        senderid = channel.userid
        comm.sender = senderid
        return self.fetch_user(comm)

    def fetch_user(self, comm):
        user_det = UserDetails(_id = comm.receiver)
        group_det = GroupDetails(_id = comm.receiver)
        user_details = self.fetch_records(user_det)
        group_details = self.fetch_records(group_det)
        res = UserDetails()
        if user_details.rowcount > 0:
            res = UserDetails()
            for row in user_details.affectedrows:
                res = UserDetails(**row)

        if group_details.rowcount > 0:
            res = GroupDetails()
            for row in group_details.affectedrows:
                res = GroupDetails(**row)
        return res
                

    def fetch_message(self, reference):
        message = Message(_id = reference.msgid)
        message_details = self.fetch_records(message)
        res = Message()
        if message_details.rowcount > 0:
            for row in message_details.affectedrows:
                res = Message(**row)
        return res
        
        

    def fetch_message_ref(self, comm):
        msg_ref = MessageReference(_id = comm.msgid)
        message_details = self.fetch_records(msg_ref)
        res = Message()
        if message_details.rowcount > 0:
            for row in message_details.affectedrows:
                res = MessageReference(**row)
        message = self.fetch_message(res)
        res.msgid = message
        return res

    def fetch_communications(self, response):
        communication = Communicator(_id = response.commid)
        communication_details = self.fetch_records(communication)
        comm = Communicator()
        for row in communication_details.affectedrows:
            comm = Communicator(**row)
        user = self.fetch_channel(comm)
        comm.sender = user
        msg_ref = self.fetch_message_ref(comm)
        comm.msgid = msg_ref
        response.commid = comm
        return response
        
            


#cgitb.enable()
config_file = "temp/project/file.json"

class Response(object):
    def __init__(self, **kwargs):
        self._response = kwargs.get('response')
        self._request_id = kwargs.get('request_id')
        self._response_type = kwargs.get('response_type', 'error')
        
    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, response):
        self._response = response

    @property
    def request_id(self):
        return self._request_id
    
    @request_id.setter
    def request_id(self, request_id):
        self._request_id = request_id

    @property
    def response_type(self):
        return self._response_type
    
    @response_type.setter
    def response_type(self, response_type):
        self._response_type = response_type



def dumpobj(respobj):
    if isinstance(respobj, list) and not isinstance(respobj, str):
        return map(dumpobj, respobj)
    # if not isinstance(respobj, Model):
    #     return respobj
    dic = respobj.__dict__
    for ob in dic:
        if isinstance(dic[ob], Model):
            dic[ob] = dumpobj(dic[ob])
        if isinstance(dic[ob], list) and not isinstance(dic[ob], str):
            dic[ob] = map(dumpobj, dic[ob])
    return json.loads(json.dumps(dic))
    


def main(args):
    config = ConfigReader(config_file).config
    dbcon = DBContext(config.dbContext.dbname, "", config.dbContext)
    sql = Repository(dbcon)
    respf = responsefetcher(sql)
    cookie=Cookie.SimpleCookie()
    # response = Response()
    if 'HTTP_COOKIE' in os.environ:
        cookie_string=os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)

    cid = 67
    status = 500
    response = json.dumps([])
    if not ('CID' in cookie and cookie['CID'].value):
        print config.resStructure.format(status=status, contype=config.requestResolver.responseHeaders.contentType, body=response)
        sys.exit(0)

    cid = cookie['CID'].value 
    try:
        response = json.dumps(dumpobj(respf.fetch_responses(cid)))
        status = 200
    except Exception as e:
        response = json.dumps([])
        
    print config.resStructure.format(status=status, contype=config.requestResolver.responseHeaders.contentType, body=response)

        


if __name__ == "__main__":
    main(sys.argv)
