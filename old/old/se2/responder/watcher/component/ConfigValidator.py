

'''
 config validator validates the user input
 config file. It applies the given context
 and matches if the required arguments are
 present in the given input config or not
'''
class ConfigValidator(object):
    def __init__(self, config, context):
        self._config = config
        self._context = context

    def validator(self):
        return True
