from watcher.models.Communicator import Communicator;
from watcher.models.RequestModel import RequestModel;

def communicatorObj(rCom, msgid):
    return Communicator(
        msgid,
        rCom.receiver,
        rCom.sender,
        rCom.status
    );


def requestObj(rReq, commid):
    return RequestModel(
        rReq.id,
        commid
    );
