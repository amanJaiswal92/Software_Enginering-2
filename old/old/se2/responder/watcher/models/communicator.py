from watcher.models.Model import Model

class RequestModel(Model):
    def __init__(self, _sender, _receiver, _status='Arrived', _msgid=None):
        self._sender = _sender;
        self._receiver   = _receiver;
        self._status   = _status;
        self._msgid   = _msgid;

    @property
    def sender(self):
        return self._sender;

    @sender.setter
    def sender(self, sender):
        self._sender = sender;
 
    @property
    def receiver(self):
        return self._receiver;

    @receiver.setter
    def receiver(self, receiver):
        self._receiver = receiver;
        
    @property
    def msgid(self):
        return self._msgid;

    @msgid.setter
    def msgid(self, msgid):
        self._msgid = msgid;
        
    @property
    def status(self):
        return self._status;

    @status.setter
    def status(self, status):
        self._status = status;

