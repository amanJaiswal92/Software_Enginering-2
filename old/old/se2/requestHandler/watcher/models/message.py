from watcher.models.Model import Model



class Message(Model):
    def __init__(self, _message, _msgtype="text", _id = None):
        self._id = _id;
        self._message = _message
        self._msgtype = _msgtype

    @property
    def message(self):
        return self._message;

    @property
    def msgtyep(self):
        return self._msgtype;
