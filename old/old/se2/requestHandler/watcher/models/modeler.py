import json
import yaml
from watcher.models.Model import Model


class RawModel(Model):
    def __init__(self, config):
        for key in config:
            if isinstance(config[key], dict):
                config[key] = RawModel(config[key])
        self.__dict__ = config
         


class modeler(object):
    def __init__(self, fconfig):
        try:
            fwconfig = json.loads(fconfig)
        except Exception as e:
            raise Exception(e)
        
        config = RawModel(fwconfig)
        self._config = config

    @property
    def model(self):
        return self._config
