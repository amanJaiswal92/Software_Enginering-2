from watcher.models.Model import Model
from watcher.models.message import Message;


class Communicator(Model):
    def __init__(self, _receiver, _sender, _message = None, _status = "Arrived", _id = None, _respid = None):
        self._id = _id;
        self._respid = _respid;
        self._message = None
        if not _message == None:
            message = _message.__dict__ 
            self._message = Message(**message);
        self._sender = _sender
        self._receiver = _receiver
        self._status = _status

    @property
    def respid(self):
        return self._respid;
    @property
    def message(self):
        return self._message;

    @property
    def sender(self):
        return self._sender;
    
    @property
    def receiver(self):
        return self._receiver;

    @property
    def status(self):
        return self._status


class RequestModel(Model):
    def __init__(self, _id, _communicator):
        self._id = _id;
        communicator = _communicator.__dict__;
        self._communicator = Communicator(**communicator)
 
    @property
    def communicator(self):
        return self._communicator;

    @communicator.setter
    def communicator(self, communicator):
        self._communicator = communicator;
