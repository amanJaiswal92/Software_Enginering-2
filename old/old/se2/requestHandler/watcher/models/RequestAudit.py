class RequestAuditModel(object):
    def __init__(self, requestid, responded = 0):
        self._requestid = requestid
        self._responded = responded;
    
    @property
    def requestid(self):
        return self._requestid;

    @requestid.setter
    def requestid(self, requestid):
        self._requestid = requestid
 
    @property
    def responded(self):
        return self._responded;

    @responded.setter
    def responded(self, responded):
        self._responded = responded
