from watcher.models.Model import Model



class Communicator(Model):
    def __init__(self, _msgid, _receiver, _sender, _status = "Arrived", _id = None, _respid = None):
        self._id = _id;
        self._respid = _respid
        self._msgid = _msgid
        self._sender = _sender
        self._receiver = _receiver
        self._status = _status

    @property
    def msgid(self):
        return self._msgid;

    @property
    def sender(self):
        return self._sender;
    
    @property
    def receiver(self):
        return self._receiver;

    @property
    def status(self):
        return self._status
    
    @property
    def respid(self):
        return self._respid
