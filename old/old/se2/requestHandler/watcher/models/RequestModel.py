from watcher.models.Model import Model


class RequestModel(Model):
    def __init__(self, _id, _commid):
        self._id = _id;
        self._commid   = _commid;
 
    @property
    def commid(self):
        return self._commid;

    @commid.setter
    def commid(self, commid):
        self._commid = commid;
