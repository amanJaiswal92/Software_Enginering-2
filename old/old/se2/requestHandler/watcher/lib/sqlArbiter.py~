import sqlite3
import MySQLdb
import MySQLdb.cursors

class SQLArbiterResponse(object):
    def __init__(self, affectedrows, rowcount, lastid):
        self._affectedrows = affectedrows
        self._rowcount = rowcount
        self._lastid   = lastid


    @property
    def lastid(self):
        return self._lastid;

    @lastid.setter
    def lastid(self, lastid):
        self._lastid = lastid

    @property
    def affectedrows(self):
        return self._affectedrows;

    @affectedrows.setter
    def affectedrows(self, affectedrows):
        self._affectedrows = affectedrows;

    @property
    def rowcount(self):
        return self._rowcount;

    @rowcount.setter
    def rowcount(self, rowcount):
        self._rowcount = rowcount;


class SQLArbiterContext(object):
    def __init__(self, dbname, sqlstmts, config):
        self._dbname = dbname;
        self._sqlstmts = sqlstmts
        self._config = config

    @property
    def config(self):
        return self._config;

    @config.setter
    def config(self, config):
        self._config = config

    @property
    def dbname(self):
        return self._dbname

    @dbname.setter
    def dbname(self, dbname):
        self._dbname = dbname

    @property
    def sqlstmts(self):
        return self._sqlstmts

    @sqlstmts.setter
    def sqlstmts(self, sqlstmts):
        self._sqlstmts = sqlstmts

'''
 SQLArbiter is the mediator between SQL and our
 python module. All classes dealing with different
 DBs should extend this to simplify the stuff.
'''
class SQLArbiter(object):
    connection = None;
    dbcontext = None
    ''' 
     Constructor must create a connection for
     provided db file and execute the sqlfile
     for base setup of db provided in the sql
     file.
    '''
    def __init__(self, dbcontext):
        self.dbcontext = dbcontext;
        config = dbcontext.config
        host = config.host
        username = config.username
        password = config.password
        dbname = config.dbname
        try:
            connection = MySQLdb.connect(host, username, password, dbname)
            self.connection = connection
        except Exception as e:
            raise Exception(e);

        sqlstmts = dbcontext.sqlstmts.split('$$')

        map(self.executeSql, sqlstmts)

    def executeSqlFile(self, sql):
        connection = self.connection;
        try:
            cursor = connection.cursor();
            for sqlc in sql:
                if not sqlc.strip():
                    cursor.execute(sqlc);
            cursor.close();

        except Exception as e:
            print e
            raise Exception(e)
    '''
     SQL Query executor for private connection
     an instance is a connection between db and
     python modules
    '''
    def executeSql(self, query_sql):
        connection = self.connection;
        response = None;

        if not query_sql.strip():
            return response;
        
        # Raise exception if connection is not yet initialized
        if not connection:
            raise Exception("Connection used before creation");
        
        
        cursor = connection.cursor(MySQLdb.cursors.DictCursor);
        cursor.execute(query_sql);
        connection.commit();
        cursor.fetchall();
        cursor.rowcount;
        response = SQLArbiterResponse(cursor, cursor.rowcount, connection.insert_id());
        return response;
