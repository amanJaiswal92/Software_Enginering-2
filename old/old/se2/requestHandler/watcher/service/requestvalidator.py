# Imports start

# To-Do: optimize imports
import os
import sys
import logging
import glob
import time
import traceback
import json
import sqlite3



#from FilewatchModel import WatchModel;
from watcher.models.FilewatchModel import WatchModel
from watcher.models.RequestAudit import RequestAuditModel
from watcher.models.request import RequestModel
from watcher.models.modeler import modeler
from watcher.models.Model import Model
import watcher.utils.wrappers as wrapper
# Imports end

class Validator(object):

    def __init__(self, repo):
        self.repo = repo
        
    def saveRecord(self, dataModel):
        return self.repo.insertValues(dataModel);

    def dbCall(self, dataModel):
        try:
            self.saveRecord(dataModel)
        except Exception as e:
            print e

    def fetchRecords(self, dataModel):
        return self.repo.selectValues(dataModel)

    def processRows(self, row):
        fileDataModel = WatchModel(**row); 
        model = modeler(fileDataModel.filecontent).model
        requestModel = RequestModel(**(model.__dict__))    
        sqlresp = self.saveRecord(requestModel.communicator.message);
        communicator = wrapper.communicatorObj(requestModel.communicator, sqlresp.lastid);
        sqlresp = self.saveRecord(communicator);
        request = wrapper.requestObj(requestModel, sqlresp.lastid);
        sqlresp = self.saveRecord(request);
        fileDataModel.compressed = 1
        self.saveRecord(fileDataModel)
        return sqlresp;
        
        
    def watch(self, rows):
        return map(self.processRows, rows);
        # for row in rows:
        #     fileDataModel = WatchModel(**row);
        #     oriRequest = None;
            
        #     try:
        #         jsonRequest = json.loads(fileDataModel.filecontent)
        #         oriRequest = RequestModel(**jsonRequest)
        #         self.dbCall(oriRequest);
        #     except Exception as e:
        #         raise Exception(e)
                
        #     fileDataModel.compressed = 1
        #     self.dbCall(fileDataModel)
            
