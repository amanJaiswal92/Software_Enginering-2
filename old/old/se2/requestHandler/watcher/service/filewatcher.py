# Imports start

# To-Do: optimize imports
import os
import sys
import logging
import glob
import time
import traceback
import json
import sqlite3



#from FilewatchModel import WatchModel;
from watcher.models.FilewatchModel import WatchModel
from watcher.models.RequestAudit import RequestAuditModel

# Imports end


def getFileName(fileName, extension):
    extension = "." + extension;
    fName = (fileName[1:]).split(extension, 1)[0]
    fName = fName + extension;
    return fName

def getFilePriority(fileName):
    last = fileName.split('.')
    priority = int(last[len(last) - 1])
    return priority
    


def setupDirectory(path):
    if not os.path.exists(path):
        os.makedirs(path);


'''
 - To-Do#1: Move this to Utils
 - To-Do#2: Change logic to handle large files in blocks
'''
def readFile(fileName):
    fileBytes = open(fileName, "rb")
    data = fileBytes.read();
    fileBytes.close();
    return data;

'''
 - To-Do: Move this to utils
'''
def markFile(filename, suffix):
    os.rename(filename, filename+suffix)

class Watchman(object):
    def __init__(self, repo):
        self.repo = repo
        
    def saveRecord(self, dataModel):
        self.repo.insertValues(dataModel);

    def dbCall(self, dataModel):
        try:
            self.saveRecord(dataModel)
        except Exception as e:
            print e

    def fetchRecords(self, dataModel):
        return self.repo.selectValues(dataModel)
        
    def watch(self):
        requestModel = WatchModel(None, None, None, 0);
        sqlResponse = self.fetchRecords(requestModel)
        cursor = sqlResponse.affectedrows
        return [row for row in cursor]
        # for row in cursor:
        #     wmodel = WatchModel(**row);
        #     originalRequest = wmodel.filecontent
        #     wmodel.compressed = 1;
        #     self.dbCall(wmodel);

