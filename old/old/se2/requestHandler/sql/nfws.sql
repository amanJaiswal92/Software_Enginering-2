CREATE TABLE IF NOT EXISTS UserTypes (
       `_usertype` VARCHAR(10) PRIMARY KEY
)$$

CREATE TABLE IF NOT EXISTS UserDetails (
       `_emailid`   VARCHAR(100) PRIMARY KEY,
       `_password`  TEXT NOT NULL,
       `_firstname` TEXT NOT NULL,
       `_lastname`  TEXT,
       `_dob`       DATE,
       `_usertype`  VARCHAR(10),
       FOREIGN KEY (`_usertype`) REFERENCES `UserTypes`(`_usertype`)
)$$

CREATE TABLE IF NOT EXISTS MessageTypes (
       `_msgtype` VARCHAR(20) PRIMARY KEY
)$$

CREATE TABLE IF NOT EXISTS Message (
       `_id`   INT AUTO_INCREMENT PRIMARY KEY,
       `_message` TEXT NOT NULL,
       `_msgtype` VARCHAR(20) NOT NULL,
       FOREIGN KEY (`_msgtype`) REFERENCES `MessageTypes`(`_msgtype`)
)$$

CREATE TABLE IF NOT EXISTS MessageStatus (
       `_status` VARCHAR(20) PRIMARY KEY
)$$

CREATE TABLE IF NOT EXISTS Communicator (
       `_id`      INT AUTO_INCREMENT PRIMARY KEY,
       `_msgid`   INT,
       `_sender`  VARCHAR(100),
       `_receiver` VARCHAR(100),
       `_status`  VARCHAR(20),
       FOREIGN KEY (`_msgid`) REFERENCES `Message`(`_id`),
       FOREIGN KEY (`_sender`) REFERENCES `UserDetails`(`_emailid`),
       FOREIGN KEY (`_receiver`) REFERENCES `UserDetails`(`_emailid`),
       FOREIGN KEY (`_status`) REFERENCES `MessageStatus`(`_status`)
)$$

CREATE TABLE IF NOT EXISTS RequestModel (
       `_id` VARCHAR(100) PRIMARY KEY,
       `_commid`    INT,
       FOREIGN KEY (`_commid`) REFERENCES `Communicator`(`_id`)
)$$

CREATE TABLE IF NOT EXISTS Responses (
       `_id` INT AUTO_INCREMENT PRIMARY KEY,
       `_requestid`  VARCHAR(100),
       FOREIGN KEY (`_requestid`) REFERENCES `RequestModel`(`_id`)
)$$

CREATE TABLE IF NOT EXISTS ResponseMsgs (
       `_responseid` INT,
       `_commid`     INT,
       PRIMARY KEY(`_responseid`, `_commid`),
       FOREIGN KEY (`_responseid`) REFERENCES `Responses`(`_id`),
       FOREIGN KEY (`_commid`) REFERENCES `Communicator`(`_id`)
)$$

DROP TRIGGER `watcher_responder`$$

CREATE TRIGGER `watcher_responder`
AFTER INSERT ON `RequestModel`
FOR EACH ROW
BEGIN
SET @commid = NEW.`_commid`;
SET @sender = (SELECT `_sender` FROM `Communicator` WHERE `_id` = NEW.`_commid` AND `_receiver`='Watcher' AND `_status` = 'Arrived' ORDER BY `_id` DESC LIMIT 1);
IF NOT ISNULL(@sender) THEN
SET @reqid = NEW.`_id`;
INSERT INTO Responses(`_requestid`) VALUES(@reqid);
SET @respid=(SELECT LAST_INSERT_ID());
INSERT INTO `ResponseMsgs`(`_responseid`, `_commid`) SELECT @respid, `_id` FROM `Communicator` WHERE `_receiver` = @sender;
UPDATE `Communicator` SET `_status` = "Delivered" WHERE `_id` = NEW.`_commid`;
END IF;
END$$



REPLACE INTO MessageTypes VALUES('text')$$
REPLACE INTO MessageTypes VALUES('multimedia')$$

REPLACE INTO MessageStatus VALUES('Arrived')$$
REPLACE INTO MessageStatus VALUES('Delivered')$$
REPLACE INTO MessageStatus VALUES('Read')$$

REPLACE INTO UserTypes VALUES('User')$$
REPLACE INTO UserTypes VALUES('Backend')$$

REPLACE INTO UserDetails VALUES('lalit', 'pass', 'lalit', 'lalit', '06-06-1994', 'User');
