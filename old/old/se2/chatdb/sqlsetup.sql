DELIMITER $$

DROP DATABASE newdb2 $$

CREATE DATABASE newdb2 $$

USE newdb2 $$

CREATE TABLE IF NOT EXISTS WatchModel(
       `_filename` VARCHAR(255) PRIMARY KEY,
       `_filecontent` TEXT,
       `_priority` INT,
       `_compressed` BOOLEAN
)$$

CREATE TABLE UserTypes (
       `_usertype` VARCHAR(10) PRIMARY KEY
)$$

CREATE TABLE UserDetails (
       `_emailid`   VARCHAR(100) PRIMARY KEY,
       `_password`  TEXT NOT NULL,
       `_firstname` TEXT NOT NULL,
       `_lastname`  TEXT,
       `_dob`       DATE,
       `_usertype`  VARCHAR(10),
       FOREIGN KEY (`_usertype`) REFERENCES `UserTypes`(`_usertype`)
)$$

CREATE TABLE Channel (
       `_id` INT AUTO_INCREMENT PRIMARY KEY,
       `_emailid` VARCHAR(100),
       FOREIGN KEY (`_emailid`) REFERENCES `UserDetails`(`_emailid`)
)$$
       

CREATE TABLE MessageTypes (
       `_msgtype` VARCHAR(20) PRIMARY KEY
)$$

CREATE TABLE Message (
       `_id`      INT           AUTO_INCREMENT PRIMARY KEY,
       `_message` TEXT          NOT NULL,
       `_msgtype` VARCHAR(20)   NOT NULL,
       FOREIGN KEY (`_msgtype`) REFERENCES `MessageTypes`(`_msgtype`)
)$$

CREATE TABLE MessageStatus (
       `_status` VARCHAR(20) PRIMARY KEY
)$$


CREATE TABLE Communicator (
       `_id`        INT AUTO_INCREMENT PRIMARY KEY,
       `_msgid`     INT,
       `_sender`    INT,
       `_receiver`  INT,
       `_status`    VARCHAR(20),
       `_respid`    INT,
       `_timestamp` TIMESTAMP NOT NULL DEFAULT
       		    	      CURRENT_TIMESTAMP
       		    	      ON UPDATE
			      CURRENT_TIMESTAMP,
       FOREIGN KEY (`_msgid`)    REFERENCES `Message`(`_id`),
       FOREIGN KEY (`_sender`)   REFERENCES `Channel`(`_id`),
       FOREIGN KEY (`_receiver`) REFERENCES `Channel`(`_id`),
       FOREIGN KEY (`_status`)   REFERENCES `MessageStatus`(`_status`)
)$$

CREATE TABLE Requests (
       `_id`     VARCHAR(100)  PRIMARY KEY,
       `_commid` INT,
       FOREIGN KEY (`_commid`) REFERENCES `Communicator`(`_id`)
)$$


CREATE TABLE Responses (
       `_id`        INT AUTO_INCREMENT PRIMARY KEY,
       `_requestid` VARCHAR(100),
       FOREIGN KEY (`_requestid`) REFERENCES `Requests`(`_id`)
)$$


CREATE TABLE ResponseMsgs (
       `_responseid` INT,
       `_commid`     INT,
       PRIMARY KEY(`_responseid`, `_commid`),
       FOREIGN KEY (`_responseid`) REFERENCES `Responses`(`_id`),
       FOREIGN KEY (`_commid`) REFERENCES `Communicator`(`_id`)
)$$


CREATE TRIGGER `watcher_responder`
AFTER INSERT ON `Requests`
FOR EACH ROW
BEGIN
SET @commid = NEW.`_commid`;
SET @postmanid = (SELECT `_id` FROM `Channel` WHERE `_emailid` = 'postman@backend.com');
SET @watcherid =(SELECT `_id` FROM `Channel` WHERE `_emailid`='watcher@backend.com');
SET @channelmanager = (SELECT `_id` FROM `Channel` WHERE `_emailid`='channelmanager@backend.com');
SET @sender = (SELECT `_sender` FROM `Communicator` WHERE `_id` = NEW.`_commid` AND `_receiver`=@watcherid AND `_status` = 'Arrived' ORDER BY `_id` DESC LIMIT 1);
IF NOT ISNULL(@sender) THEN
SET @reqid = NEW.`_id`;
INSERT INTO Responses(`_requestid`) VALUES(@reqid); SET @respid=(SELECT LAST_INSERT_ID());
INSERT INTO `ResponseMsgs`(`_responseid`, `_commid`) SELECT @respid, `_id` FROM `Communicator` WHERE `_receiver` = @sender;
UPDATE `Communicator` SET `_status` = "Delivered" WHERE `_id` = NEW.`_commid`;
END IF;
SET @respid = (SELECT `_respid` FROM `Communicator` WHERE `_id` = @commid AND `_receiver`=@postmanid AND `_status` = 'Arrived');
IF NOT ISNULL(@respid) THEN
UPDATE `Communicator` SET `_status` = 'Delivered' WHERE `_id` IN (SELECT `_commid` FROM `ResponseMsgs` WHERE `_responseid` = @respid);
UPDATE `Communicator` SET `_status` = 'Delivered' WHERE `_id` = @commid;
END IF;
SET @sender = (SELECT `_sender` FROM `Communicator` WHERE `_id` = NEW.`_commid` AND `_receiver`=@channelmanager AND `_status` = 'Arrived' ORDER BY `_id` DESC LIMIT 1);
IF NOT ISNULL(@sender) THEN
SET @reqid = NEW.`_id`;
INSERT INTO Responses(`_requestid`) VALUES(@reqid);
SET @respid=(SELECT LAST_INSERT_ID());
INSERT INTO Channel(`_emailid`) VALUES();
INSERT INTO `Message`(`_message`, `_msgtype`) VALUES(

INSERT INTO `ResponseMsgs`(`_responseid`, `_commid`) SELECT @respid, `_id` FROM `Communicator` WHERE `_receiver` = @sender;
UPDATE `Communicator` SET `_status` = "Delivered" WHERE `_id` = NEW.`_commid`;
END IF;

END $$




INSERT INTO UserTypes                VALUES ('Backend'),('User') $$
INSERT INTO UserDetails              VALUES('registeration@backend.com', 'randomstr', 'Registeration', 'Service', NULL, 'Backend')$$
INSERT INTO UserDetails              VALUES('watcher@backend.com', 'randomstr', 'Watcher', 'Service', NULL, 'Backend')$$
INSERT INTO UserDetails              VALUES('postman@backend.com', 'randomstr', 'Postman', 'Service', NULL, 'Backend')$$
INSERT INTO MessageTypes             VALUES('multimedia')$$
INSERT INTO MessageTypes             VALUES('text')$$
INSERT INTO Channel(`_emailid`)      VALUES('watcher@backend.com')$$
INSERT INTO Channel(`_emailid`)      VALUES('postman@backend.com')$$
INSERT INTO MessageStatus(`_status`) VALUES('Arrived'),('Delivered')$$


