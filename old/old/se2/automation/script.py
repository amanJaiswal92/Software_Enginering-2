import subprocess
import json

var = [
    {
        "command": "ovs-vsctl",
        "args": ["show", "asdf"]
    }
]

bridgename = "br-int"


fp = open("config-setup-ovs.json", "r")
commandjson = json.load(fp)

# Setup ovs db

commandlst = commandjson["db_setup"]["command"]
commandlst = [commandlst] + commandjson["db_setup"]["args"]

subprocess.call(commandlst)

# Add-bridge

commandlst = commandjson["add_br"]["command"]
commandlst = [commandlst, bridgename]

subprocess.call(commandlst)
