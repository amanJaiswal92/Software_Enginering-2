class RequestModel(object):
    def __init__(self, _requestid, _responseid, _response):
        self._requestid = _requestid;
        self._response  = _responseid;
        self._response    = _response;
    
    @property
    def requestid(self):
        return self._requestid;

    @requestid.setter
    def requestid(self, requestid):
        self._requestid = requestid
 
    @property
    def responseid(self):
        return self._responseid;

    @responseid.setter
    def responseid(self, responseid):
        self._responseid = responseid

    @property
    def response(self):
        return self._response;

    @response.setter
    def response(self, response):
        self._response = response
