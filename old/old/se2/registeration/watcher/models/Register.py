from watcher.models.Model import Model

class UserDetails(Model):
    def __init__(self, _emailid, _password, _dob, _firstname, _lastname, _usertype = "User"):
        self._dob      = _dob
        self._emailid    = _emailid;
        self._firstname = _firstname;
        self._lastname = _lastname;
        self._password = _password;

    @property
    def password(self):
        return self._password;

    @password.setter
    def password(self, password):
        self._password = password;
        
    @property
    def emailid(self):
        return self._emailid;

    @emailid.setter
    def emailid(self, emailid):
        self._emailid = emailid

    @property
    def firstname(self):
        return self._firstname;

    @firstname.setter
    def firstname(self, firstname):
        self._firstname = firstname
        
    @property
    def lastname(self):
        return self._lastname;

    @lastname.setter
    def lastname(self, lastname):
        self._lastname = lastname
        
    @property
    def dob(self):
        return self._dob;

    @dob.setter
    def dob(self, dob):
        self._dob = dob;
