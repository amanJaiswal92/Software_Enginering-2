class ErrorModels():
    def __init__(self, _responseid, _errorid=None):
        self._responseid=_responseid;
        self._errorid = _errorid

    @property
    def responseid(self):
        return self._responseid

    @responseid.setter
    def responseid(self, _responseid):
        self._responseid = _responseid;

    @property
    def errorid(self):
        return self._errorid

    @errorid.setter
    def errorid(self, _errorid):
        self._errorid = _errorid;

    @staticmethod
    def errorsForResponseId(responseid, repo):
        errorModel = ErrorModels(responseid);
        errors = repo.selectValues(errorModel)
        cursor = errors.affectedrows
        lst = [row for row in cursor]
        errorModels = []
        for em in  lst:
            errorModels.append(ErrorModels(**em))
        return errorModels
