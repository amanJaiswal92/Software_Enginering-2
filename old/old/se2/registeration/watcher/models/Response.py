from watcher.models.Model import Model


class Message(Model):
    def __init__(self, _message, _msgtype, _id = None):
        self._id = _id;
        self._message = _message
        self._msgtype = _msgtype

    @property
    def id(self):
        return self._id

    @property
    def message(self):
        return self._message;

    @property
    def msgtyep(self):
        return self._msgtype;


class Communicator(Model):
    def __init__(self, _receiver, _sender, _respid = None, _message = None, _status = "Arrived", _id = None):
        self._id = _id;
        self._respid = _respid;
        self._message = _message;
        self._sender = _sender
        self._receiver = _receiver
        self._status = _status

    @property
    def respid(self):
        return self._respid;
    
    @property
    def message(self):
        return self._message;

    @property
    def sender(self):
        return self._sender;
    
    @property
    def receiver(self):
        return self._receiver;

    @property
    def status(self):
        return self._status


class RequestModel(Model):
    def __init__(self, _communicator, _id = None):
        self._id = _id;
        self._communicator = _communicator
 
    @property
    def communicator(self):
        return self._communicator;

    @communicator.setter
    def communicator(self, communicator):
        self._communicator = communicator;
