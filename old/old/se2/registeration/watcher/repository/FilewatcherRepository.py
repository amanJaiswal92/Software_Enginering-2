import sys
import json
from watcher.config.FileWatcherConfig import fwconfig;
#from watcher.models.FilewatchModel import WatchModel;
from watcher.lib.sqlArbiter import SQLArbiter
from watcher.lib.sqlArbiter import SQLArbiterContext
fconfig = fwconfig["sql"]


# To-do: Call this from filewatcher module

# To-do: move this to commmon utility
def listToQuotedStrings(lst, delim):
    return ','.join([(delim + '{}' + delim).format(value) for value in lst])



'''
- FilewatcherRepository extends SQLArbiter
- UseCase: a plug between python application and SQL
  for filewatcher
- Usage: FilewatchRepository(<dbname|dbpath>)
'''

class FilewatchRepository(SQLArbiter):
    # Called for setting up database using sql file in specified DB.
    def setupDB(self, sqlstmts):
        super(FilewatchRepository, self).executeSql(sqlstmts);

    # Called for inserting/updating data in the table
    def insertValues(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel);
        if(len(attrs) == 0):
            raise Exception("Nothing to insert")
        tableName = watchModel.__class__.__name__;
        column_names = [x for x in attrs]
        column_names = listToQuotedStrings(column_names, '`')
        column_values = [attrs[x] for x in attrs]
        column_values = map(json.dumps, column_values)
        column_values = ','.join(column_values);
        query_sql = config.insertquery.format(tablename=tableName,columnname=column_names, values=column_values);
        super(FilewatchRepository, self).executeSql(query_sql);

    # Called for inserting/updating data in the table
    def updateValues(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel);
        if(len(attrs) == 0):
            raise Exception("Nothing to insert")
        tableName = watchModel.__class__.__name__;
        column_names = [x for x in attrs]
        column_names = listToQuotedStrings(column_names, '`')
        column_values = [attrs[x] for x in attrs]
        column_values = map(json.dumps, column_values)
        column_values = ','.join(column_values);
        query_sql = config.replacequery.format(tablename=tableName,columnname=column_names, values=column_values);
        super(FilewatchRepository, self).executeSql(query_sql);
 
 
    # Called for searching data
    def selectValues(self, watchModel, tableName = None):
        config = self.dbcontext.config
        tableName = tableName or watchModel.__class__.__name__;
        attrs = vars(watchModel);
        attrs = {key: value for key, value in attrs.iteritems() if not value == None}
        conditions = " and ".join(["=".join(['`{}`'.format(key), '\'{}\''.format(str(val))]) for key, val in attrs.items()])
        if conditions:
            conditions = "WHERE " + conditions
        query_sql = config.selectquery.format(tablename=tableName, conditions=conditions)
        return super(FilewatchRepository, self).executeSql(query_sql);

        
