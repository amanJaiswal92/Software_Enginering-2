from watcher.models.Model import Model
import json
def jsonify(model):
    dic = model.__dict__
    for ob in dic:
        if isinstance(dic[ob], Model):
            dic[ob] = jsonify(dic[ob])
        if isinstance(dic[ob], list) and not isinstance(dic[ob], str):
            dic[ob] = map(jsonify, dic[ob])
    return json.loads(json.dumps(dic))
                                        
