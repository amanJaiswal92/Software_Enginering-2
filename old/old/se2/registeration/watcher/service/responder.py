# Imports start

# To-Do: optimize imports
import os
import sys
import logging
import glob
import time
import traceback
import json
import sqlite3
import requests


#from FilewatchModel import WatchModel;
from watcher.models.FilewatchModel import WatchModel
from watcher.models.Request import Communicator
from watcher.models.Request import RequestModel
from watcher.models.Request import Message

import watcher.utils.jsonify as jsonify
# Imports end



class Responder(object):
    def __init__(self, repo):
        self.repo = repo

    def postRequest(self, url, requestBody):
        r = requests.post(url, requestBody);
        return r

    def sendMsgs(self, message):
        return self.postRequest(self.repo.url, message);
    
    def watch(self, responses):
        communications = map(self.sendMsgs, responses);
        return communications;
