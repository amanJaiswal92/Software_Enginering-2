# Imports start

# To-Do: optimize imports
import os
import sys
import logging
import glob
import time
import traceback
import json
import sqlite3
import MySQLdb


#from FilewatchModel import WatchModel;
from watcher.models.Model import Model;
from watcher.models.FilewatchModel import WatchModel
from watcher.models.RequestAudit import RequestAuditModel
from watcher.models.RequestModel import RequestModel
from watcher.models.Register import UserDetails
from watcher.models.errors import ErrorModels;

# Imports end

    



def getFileName(fileName, extension):
    extension = "." + extension;
    fName = (fileName[1:]).split(extension, 1)[0]
    fName = fName + extension;
    return fName

def getFilePriority(fileName):
    last = fileName.split('.')
    priority = int(last[len(last) - 1])
    return priority
    


def setupDirectory(path):
    if not os.path.exists(path):
        os.makedirs(path);


'''
 - To-Do#1: Move this to Utils
 - To-Do#2: Change logic to handle large files in blocks
'''
def readFile(fileName):
    fileBytes = open(fileName, "rb")
    data = fileBytes.read();
    fileBytes.close();
    return data;

'''
 - To-Do: Move this to utils
'''
def markFile(filename, suffix):
    os.rename(filename, filename+suffix)

def makeResponse(status, userid, message):
    mymsg = {
        "status": status,
        "userid": userid,
        "message": message,
    }
    return json.dumps(mymsg);

def makeResponseObj(response, receiver, sender):
    # response = json.dumps(response)
    resp = {
        "_communicator": {
            "_receiver": receiver,
            "_sender"  : sender,
            "_message" : {
                "_msgtype": "text",
                "_message": response
            }
        }
    }
    return json.dumps(resp);
    
    
class Validator(object):
    def __init__(self, repo):
        self.repo = repo
        
    def saveRecord(self, dataModel):
        url = self.repo.insertValues(dataModel);

    def fetchRecords(self, dataModel):
        return None

    def process(self, communication):
        error = "User Created Successfully";
        status = "Success";
        userid = None;
        message = communication.message;
        receiver = communication.sender;
        sender = "Registeration"
        try:
            msg = json.loads(message.message);
            udtails = UserDetails(**msg);
            ud = self.saveRecord(udtails);
            userid = udtails.emailid;
        except Exception as e:
            error = "Invalid Request " + str(e)
            status = "Failed"
            
        response = makeResponse(status, userid, error)
        response = makeResponseObj(response, receiver, sender);
        return response

        
        
    def watch(self, communications):
        return map(self.process, communications);

            
