import sys
import os
import time
import glob

from watcher.lib.daemon import Daemon
from watcher.lib.sqlArbiter import SQLArbiterContext as DBContext

from watcher.service.filewatcher import Watchman
from watcher.service.requestvalidator import Validator
from watcher.service.responder import Responder

from watcher.repository.FilewatcherRepository import FilewatchRepository as Repository

from watcher.config.configReader import ConfigReader

def initializeRepo(config, context):
    sqlfile = os.path.join(context.abspath, config.sql.sqlfile)
    with open(sqlfile, 'r') as f:
        sqlstmts = f.read()
    dbname = os.path.join(context.wd, config.sql.dbname);

    dbcontext = DBContext(dbname, sqlstmts, config.sql)
    repo = Repository(dbcontext)
    return repo;

def initializeWatchman(config, context):
    sqlfile = os.path.join(context.abspath, config.sql.sqlfile)
    with open(sqlfile, 'r') as f:
        sqlstmts = f.read()
    dbname = os.path.join(context.wd, config.sql.dbname);
    dbcontext = DBContext(dbname, sqlstmts, config.sql)
    repo = Repository(dbcontext)
    return Watchman(repo);


class WatchManager(Daemon):        
    def run(self, context):
        os.chdir(context.directory)
        if len(context.argv) <= 2:
            raise Exception("No Config File provided");
        fconfig = context.argv[2];
        config = ConfigReader(fconfig).config;
        repo = config.serverDetails
        watchman = Watchman(repo);
        responder = Responder(repo);
        repo = initializeRepo(config, context)
        validator = Validator(repo);
        while True:             
            newRequests = watchman.watch();
            responses = validator.watch(newRequests);
            responder.watch(responses)
#            validator.watch(newRequests);
            # time.sleep(config.manager.sleepDuration) 
        
