import sys

from watcher.config.FileWatcherConfig import fwconfig;

from watcher.lib.sqlArbiter import SQLArbiter

fconfig = fwconfig["sql"]


# To-do: Call this from filewatcher module

# To-do: move this to commmon utility
def listToQuotedStrings(lst):
    return ','.join(['\'{}\''.format(value) for value in lst])



'''
- FilewatcherRepository extends SQLArbiter
- UseCase: a plug between python application and SQL
  for filewatcher
- Usage: Filewatcher(<dbname|dbpath>)
'''

class FilewatchRepository(SQLArbiter):

    # Called for setting up database using sql file in specified DB.
    def setupDB(self, sqlstmts):
        super(FilewatchRepository, self).executeSql(sqlstmts);

    # Called for inserting/updating data in the table
    def insertValues(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel);
        if(len(attrs) == 0):
            raise Exception("Nothing to insert")
        tableName = watchModel.__class__.__name__;
        column_names = [x for x in attrs]
        column_names = listToQuotedStrings(column_names, '`')
        column_values = [attrs[x] for x in attrs]
        column_values = listToQuotedStrings(column_values, '\'')
        query_sql = config.insertquery.format(tablename=tableName,columnname=column_names, values=column_values);
        super(FilewatchRepository, self).executeSql(query_sql);

    # # Called for inserting/updating data in the table
    # def insertValues(self, watchModel, tableName):
    #     attrs = vars(watchModel);
    #     column_values = [x[1] for x in attrs.items()]
    #     column_values = listToQuotedStrings(column_values)
    #     query_sql = fconfig["insertquery"].format(tablename=tableName, values=column_values);
    #     super(FilewatchRepository, self).executeSql(query_sql);

    def fetchdata(self, watchModel):
        config = self.dbcontext.config
        attrs = vars(watchModel);
        query_sql = config.selectquery
        

    def fetchuncompressed(self, tableName):
        print tableName
        query_sql = fconfig["selectquery"].format(tablename="temptable", compressed="False");
        response = super(FilewatchRepository, self).executeSql(query_sql);
        return response
