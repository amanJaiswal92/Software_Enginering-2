class Response(object):
    def __init__(self, requestid, status, timeout):
        self._requestid = requestid;
        self._timeout = timeout;
        self._status = status;

    @property
    def requestid(self):
        return self._requestid

    @property
    def timeout(self):
        return self._timeout

    @property
    def status(self):
        return self._status
