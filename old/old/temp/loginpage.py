#! /usr/bin/env python2
import cgi
import cgitb
import time
import os
import json
import sys
import project.util as util
from urlparse import urljoin
from project.Config import ConfigReader

cgitb.enable()
config_file = "project/file.json"

def main(args):
    # cookie=Cookie.SimpleCookie()
    # if 'HTTP_COOKIE' in os.environ:
    #     cookie_string=os.environ.get('HTTP_COOKIE')
    #     cookie.load(cookie_string)
    # if not 'CID' in cookie:
    #     cookie['CID'] = registerUser();
    
    config = ConfigReader(config_file).config
    registerpage = open(config.logintemplate, 'r')
    html = registerpage.read();
    netloc = config.authServer.url
    port = config.authServer.port
    scheme = config.authServer.scheme
    path = config.authServer.login
    registration = util.joinurl(netloc, path, port, scheme)
    html = html.format(login=registration)
    print config.resStructure.format(status=200, contype=config.hostServer.responseHeaders.contentType, body=html)

if __name__ == "__main__":
    main(sys.argv)
