class RequestModel(object):
    def __init__(self, **kwargs):
        self._id = kwargs.get('id', None)
        self._commid = kwargs.get('commid')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        return self._id

    @property
    def commid(self):
        return self._commid

    @commid.setter
    def commid(self, _commid):
        self._commid = _commid

class Communicator(object):
    def __init__(self, **kwargs):
        self._id = kwargs.get("id");
        self._sender = kwargs.get("sender")
        self._receiver = kwargs.get("receiver")
        self._msgid = kwargs.get("msgid")
        self._status = kwargs.get("status", "Arrived")

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, _sender):
        self._sender = _sender

    @property
    def receiver(self):
        return self._receiver 

    @receiver.setter
    def receiver(self, _receiver):
        self._receiver = _receiver

    @property
    def msgid(self):
        return self._msgid

    @msgid.setter
    def msgid(self, _msgid):
        self._msgid = _msgid

    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, _status):
        self._status = _status
    
    
class Channel(object):
    def __init__(self, **kwargs):
        self._id = kwargs.get('id')
        self._userid = kwargs.get('userid')
        self._active = kwargs.get('active', True)

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def userid(self):
        return self._userid

    @userid.setter
    def userid(self, _userid):
        self._userid = _userid

    @property
    def active(self):
        return self._active

    @active.setter
    def active(self, _active):
        self._active = _active


class MessageReference(object):
    def __init__(self, **kwargs):
        self._id = kwargs.get('id')
        self._sender = kwargs.get('sender')
        self._msgid = kwargs.get('msgid')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def msgid(self):
        return self._msgid

    @msgid.setter
    def msgid(self, _msgid):
        self._msgid = _msgid

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, _sender):
        self._sender = _sender


class Message(object):
    def __init__(self, **kwargs):
        self._id = kwargs.get('id')
        self._message = kwargs.get('message')
        self._msgtype = kwargs.get('msgtype')

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = _id

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, _message):
        self._message = _message

    @property
    def msgtype(self):
        return self._msgtype

    @msgtype.setter
    def msgtype(self, _msgtype):
        self._msgtype = _msgtype
