from project.models.request import Communicator

def communicatorRespObj(comm, message):
    return Communicator(message, comm.receiver, comm.sender, comm.status, comm.id, comm.respid, comm.sendername, comm.receivername);
    
