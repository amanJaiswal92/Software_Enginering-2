from urlparse import urljoin
import requests
import time
import json

def joinurl(netloc, paths, port = 80, scheme='http'):
    netadd = scheme + "://" + netloc + ":" + str(port)
    return urljoin(netadd, paths)

