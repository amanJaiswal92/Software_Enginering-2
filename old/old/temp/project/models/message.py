from project.models.Model import Model



class Message(Model):
    def __init__(self, _message, _msgtype, _id = None, _sender = None):
        self._id = _id;
        self._message = _message
        self._msgtype = _msgtype
        self._sender = _sender;

    @property
    def message(self):
        return self._message;

    @property
    def msgtype(self):
        return self._msgtype;

    @property
    def sender(self):
        return self._sender;
