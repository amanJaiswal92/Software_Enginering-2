from project.models.Model import Model
from project.models.message import Message;


class Communicator(Model):
    def __init__(self, _message, _receiver, _sender, _status = "Arrived", _id = None, _respid = None, _sendername = None, _receivername = None):
        self._id = _id;
        self._respid = _respid;
        message = _message.__dict__;
        self._message = Message(**message);
        self._sender = _sender
        self._receiver = _receiver
        self._status = _status
        self._sendername = _sendername;
        self._receivername = _receivername;

    @property
    def respid(self):
        return self._respid
    @property
    def message(self):
        return self._message

    @property
    def receivername(self):
        return self._receivername;

    @receivername.setter
    def receivername(self, receivername):
        self._receivername = receivername;

    @property
    def sendername(self):
        return self._sendername;

    @sendername.setter
    def sendername(self, sendername):
        self._sendername = sendername;
        
    @property
    def msgid(self):
        return self._msgid;

    @property
    def sender(self):
        return self._sender;

    @property
    def sender(self):
        return self._sender;
    
    @property
    def receiver(self):
        return self._receiver;

    @property
    def status(self):
        return self._status


class RequestModel(Model):
    def __init__(self, _id, _communicator):
        self._id = _id;
        communicator = _communicator.__dict__;
        self._communicator = Communicator(**communicator)
 
    @property
    def communicator(self):
        return self._communicator;

    @communicator.setter
    def communicator(self, communicator):
        self._communicator = communicator;
