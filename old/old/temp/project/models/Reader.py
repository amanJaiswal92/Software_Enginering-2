from project.models.Model import Model

class Channel(Model):
    def __init__(self, _emailid, _id = None, _groupid = None):
        self._emailid = _emailid;
        self._id = _id;
        self._groupid = _groupid;

    @property
    def emailid(self):
        return self._emailid

    @property
    def groupid(self):
        return self._groupid

class Message(Model):
    def __init__(self, _id, _message = None, _msgtype = None, _sender = None):
        self._id = _id;
        self._message = _message
        self._msgtype = _msgtype
        self._sender = _sender;

    @property
    def message(self):
        return self._message;

    @property
    def msgtype(self):
        return self._msgtype;

    @property
    def sender(self):
        return self._sender;

class ResponseObj(Model):
    def __init__(self, _responseid, _requestid, _communications):
        self._responseid = _responseid
        self._requestid = _requestid
        self._communications = _communications

    @property
    def responseid(self):
        return self._responseid

    @property
    def requestid(self):
        return self._requestid

    @property
    def communications(self):
        return self._communications

class Responses(Model):
    def __init__(self, _requestid, _id = None):
        self._requestid = _requestid
        self._id = _id

    @property
    def requestid(self):
        return self._requestid


class ResponseMsgs(Model):
    def __init__(self, _responseid, _commid = None):
        self._commid = _commid;
        self._responseid = _responseid
    
    @property
    def responseid(self):
        return self._responseid;

    @property
    def commid(self):
        return self._commid

class Communicator(Model):
    def __init__(self, _id, _msgid = None, _receiver = None, _sender = None, _status = None, _respid = None, _timestamp = None):
        self._id = _id;
        self._msgid = _msgid
        self._sender = _sender
        self._receiver = _receiver
        self._status = _status
        self._respid = _respid
        self._timestamp = _timestamp;

    @property
    def respid(self):
        return self._respid;

    @property
    def sendername(self):
        return self._sendername;

    @sendername.setter
    def sendername(self, sendername):
        self._sendername = sendername;

    @property
    def receivername(self):
        return self._receivername;

    @receivername.setter
    def receivername(self, receivername):
        self._receivername = receivername;
        
    @property
    def msgid(self):
        return self._msgid;

    @property
    def sender(self):
        return self._sender;
    
    @property
    def receiver(self):
        return self._receiver;

    @sender.setter
    def sender(self, sender):
        self._sender = sender;

    @receiver.setter
    def receiver(self, receiver):
        self._receiver = receiver;

    @property
    def status(self):
        return self._status
