#! /usr/bin/env python2
import cgi
import json
import uuid
import sys
import traceback
import cgitb
import Cookie
import os
from project.repository.Repository import Repository


from project.models.Reader import Message
from project.models.Reader import ResponseMsgs
from project.models.Reader import Responses
from project.models.Reader import Channel
from project.models.Reader import Communicator
from project.models.Reader import ResponseObj
from project.models.Model import Model

import project.utils.ResponseWrapper as wrapper

from project.Config import ConfigReader
from requestobj import Request
from response import Response

config_file = "project/file.json"
config = ConfigReader(config_file).config
cgitb.enable()

repo = Repository(config.dbServer);

def fetchMessages(msgid):
    message = Message(msgid)
    sqlresp = repo.fetchRecords(message);
    message = [row for row in sqlresp.affectedrows][0]
    return Message(**message)

def fetchCommunications(communicationid):
    communication = Communicator(communicationid)
    sqlresp = repo.fetchRecords(communication)
    communication = [row for row in sqlresp.affectedrows][0]
    communication = Communicator(**communication);
    sender = fetchChannel(Channel(None, communication.sender)).emailid;
    receiver = fetchChannel(Channel(None, communication.receiver)).emailid;
    # communication.sender = sender;
    # communication.receiver = receiver;
    msgid = communication.msgid;
    message = fetchMessages(msgid);
    return wrapper.communicatorRespObj(communication, message)


def fetchResponseMsgs(responseid):
    responseMsgs = ResponseMsgs(responseid);
    sqlresp = repo.fetchRecords(responseMsgs);
    resps = []
    for x in sqlresp.affectedrows:
        resps.append(ResponseMsgs(**x))
    communications = map(lambda x: fetchCommunications(x.commid), resps);
    return communications

def getCommunications(rspns):
    communications = []
    if rspns.id:
        communications = fetchResponseMsgs(rspns.id)
    return communications

def fetchResponses(response):
    sqlresp = repo.fetchRecords(response)
    cursor = sqlresp.affectedrows
    rspns = cursor.fetchone();
    rspns = Responses(**rspns) if not rspns == None else response;
    communications = getCommunications(rspns)#map(getCommunications, rspns);
    return ResponseObj(rspns.id, response.requestid, communications)


def fetchChannel(channel):
    sqlresp = repo.fetchRecords(channel)
    cursor = sqlresp.affectedrows
    rspns = cursor.fetchone();
    rspns = Channel(**rspns) if not rspns == None else channel;
    return rspns;

    
def dumpobj(respobj):
    if isinstance(respobj, list) and not isinstance(respobj, str):
        return map(dumpobj, respobj)
    dic = respobj.__dict__
    for ob in dic:
        if isinstance(dic[ob], Model):
            dic[ob] = dumpobj(dic[ob])
        if isinstance(dic[ob], list) and not isinstance(dic[ob], str):
            dic[ob] = map(dumpobj, dic[ob])
    return json.loads(json.dumps(dic))
        

def main():
    request = json.load(sys.stdin);
    requestid = request["_channel"] 
    requestid = fetchChannel(Channel(None, requestid)).emailid;
    response = json.dumps({"_channel": requestid})
    print config.resStructure.format(status=200, contype=config.requestResolver.responseHeaders.contentType, body=response)


if __name__ == "__main__":
    main();
