#! /usr/bin/env python2
import binascii
import hashlib
import socket
import struct
from time import time
import Cookie
import requests
import cgi
import sys
from mod_auth import Ticket

secret = "b8fb7b6df0d64dd98b8ccd00577434d7"
class AuthTkt(object):
    def __init__(self, secret, uid, data='', tokens=(), base64=True, encoding='utf-8', timeout = 7200):
        self.secret = str(secret)
        self.uid = str(uid)
        self.data = data
        self.encoding = encoding
        self.tokens = tokens
        self.base64 = base64
        self.timeout = timeout
        
    def generatetkt(self):
        auth_Ticket = Ticket(self.secret)
        validuntil = int(time()) + self.timeout
        ticket = auth_Ticket.createTkt(self.uid, validuntil=validuntil)
        self.ticket = ticket
        return ticket

    def getcookie(self, ticket):
        cookie = Cookie.SimpleCookie()
        cookie['auth_tkt'] = binascii.b2a_base64(ticket).strip()
        return cookie

    
    def getAuthCookie(self):
        return (self.generatetkt())

responseString = "Status: {status}\nContent-type: {contype}\n\n{body}"
form = cgi.FieldStorage() # parse query
response = {
    "ticket": ""
}

if not form.has_key('uid'):
    print responseString.format(status=400, contype="application/json", body=response);
    sys.exit(0)

uid = form['uid'].value
ticket = AuthTkt(secret, uid).getAuthCookie()
response["ticket"] = str(ticket)
        
print responseString.format(status=200, contype="application/json", body=response);
# r = requests.post('http://localhost/cgi-bin/validateauth.py', data={'tkt': str(ticket)})
# print r.status_code, r.reason, json.loads(r.content)['tkt'] 
