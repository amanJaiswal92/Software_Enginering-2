#! /usr/bin/env python2
import cgi
import json
import uuid
import sys
import traceback
import cgitb
import Cookie
import os

from project.lib.sqlArbiter import SQLArbiter
from project.lib.sqlArbiter import SQLArbiterContext
from project.repository.UserDetailsRepository import UserDetailsRepository
from project.models.userdetails import UserDetails
from project.Config import ConfigReader
import project.util as util;

from requestobj import Request
from response import Response

config_file = "project/file.json"
config = ConfigReader(config_file).config
cgitb.enable()

body = """
{
    "_communicator": {
        "_receiver": "channelmanager@backend.com",
        "_sender"  : "homepage@webserver.com",
        "_message" : null
    }
}
"""

def main():
    cookie=Cookie.SimpleCookie()
    if 'HTTP_COOKIE' in os.environ:
        cookie_string=os.environ.get('HTTP_COOKIE')
        cookie.load(cookie_string)
    
    timeout = 100
    priority = 0
    status = "Submitted"
    path = "/home/lalit/assignments/senew2/requests/"
    error = status
    request = json.load(sys.stdin)
    # request = json.loads(body);

    if (not "_sender" in request["_communicator"] or request["_communicator"]["_sender"] == None) and not 'CID' in cookie :
        response = Response(None, None, None)
        response = vars(response)
        print config.resStructure.format(status=401, contype=config.requestResolver.responseHeaders.contentType, body=json.dumps(response))
        return;

    if (not "_sender" in request["_communicator"] or request["_communicator"]["_sender"] == None):
        request["_communicator"]["_senderid"] = cookie["CID"].value;



    
    # Generate Request ID
    requestId = str(uuid.uuid1())
    request["_id"] = requestId
    
    # Create a request file
    try:
        fp = open(path + requestId + ".json", "w");
        fp.write(json.dumps(request));
        fp.close();
        fp = open(path + "." + requestId + ".json." + str(priority), "w");
        fp.close();
    except Exception as e:
        error = str(e)

    # Send response
    response = Response(requestId, error, 100)
    response = vars(response)
    print config.resStructure.format(status=200, contype=config.requestResolver.responseHeaders.contentType, body=json.dumps(response))


if __name__ == "__main__":
    main();
