#! /usr/bin/env python2
import cgi

import binascii
import hashlib
import socket
import struct
from time import time
import Cookie
import sys
import os
from mod_auth import Ticket
import cgitb
cgitb.enable()
#sys.stderr = open('logs.txt', 'a')
class ValidateCookie(object):
    def __init__(self, secret):
        self.secret = str(secret)
        
    def getAuthTicket(self, cookie):
        return cookie['auth_tkt'].value
    
    def validateTkt(self, cookie):
        auth_Ticket = Ticket(self.secret)
        ticket = self.getAuthTicket(cookie)
        ticket = binascii.a2b_base64(ticket)
        return auth_Ticket.validateTkt(ticket)


responseString = "Status: {status}\nContent-type: {contype}\n\n{body}"
form = cgi.FieldStorage() # parse query
response = {
    "tkt": ""
}
secret = "b8fb7b6df0d64dd98b8ccd00577434d7"
if not form.has_key('tkt'):
    print responseString.format(status=400, contype="application/json", body=response);

    sys.exit(0)

cookie = form['tkt'].value;
c = Cookie.SimpleCookie(str(cookie))
tkt = ""
statuscode = 200

try:
    tkt = ValidateCookie(secret).validateTkt(c)
except Exception as e:
    statuscode = 401

response["tkt"] = tkt
print responseString.format(status=statuscode, contype="application/json", body=response);
    
