var waiting = false;
var messageWindowID = "messageWindow"
var messagePopUp = '<div class="talk-bubble btm-right"><div class="talktext"><p>{0}</p></div></div>'
var mysender = null;
var myreceiver = null;
var currentSelected = null;
String.prototype.format = function() {
    var s = this,
	i = arguments.length;
    while (i--) {
	s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};


function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
	hash = str.charCodeAt(i) + ((hash << str.length) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();
    return "00000".substring(0, 6 - c.length) + c;
}


function createMessagePopUp(message, left, sendername) {
    floater = left ? "" : "contact-right";
    bubble = document.createElement('div');
    text = document.createElement('div');    
    // var mname = document.createElement('div');
    mname = document.createElement('p');
    // mname.addEventListener("click", function() {
    // 	if(sendername != mysender) {
    // 	    myreceiver = sendername;
    // 	    showToast("Receiver: " + myreceiver, "success");
    // 	}
    // });
    mname.innerHTML = sendername;

    // mname.appendChild(pname);
    mname.className = "namefield";
    mname.style.color = "#" + intToRGB(hashCode(sendername));
    
    p = document.createElement('p');
    p.innerHTML = message;
    text.appendChild(p);
    text.className = "talktext"
    bubble.className = "talk-bubble btm-right " + floater;
    bubble.appendChild(mname);
    bubble.appendChild(text);
    return bubble;
}


function addMessagePopUp(message) {
    msgPopUp = createMessagePopUp(message, true, mysender);
    msgWindow = document.getElementById(myreceiver);
    if(!msgWindow) {
	msgWindow = document.createElement('div');
	msgWindow.className = "contacts-sub"
	msgWindow.setAttribute('id', myreceiver);
	superMsgWindow = document.getElementById('messageWindow');
	superMsgWindow.append(msgWindow);
	appendContact(myreceiver);
    }
    msgWindow.appendChild(msgPopUp);
    msgWindow.scrollTop = msgWindow.scrollHeight;
}


function addMessage(message, keyid) {

    messagePopUps = message.map(function(msg) {
	return createMessagePopUp(msg._message._message, msg._sendername == mysender, msg._sendername);
    });
    msgWindow = document.getElementById(messageWindowID);
    var appnd = false;
    
    subMsgWindow = document.getElementById(keyid)
    if(!subMsgWindow) {
	subMsgWindow = document.createElement('div');
	appnd = true;
    }
    subMsgWindow.className = "contacts-sub"
    subMsgWindow.setAttribute('id', keyid);
    messagePopUps.forEach(function(msgPopUp) {
	subMsgWindow.appendChild(msgPopUp);
    });
    msgWindow.appendChild(subMsgWindow);
    subMsgWindow.scrollTop = subMsgWindow.scrollHeight;
    if(appnd)
	appendContact(keyid);
}

function appendContact(keyid, contactList) {
    contactList = document.getElementById('contactlist');
    contactContainer = document.createElement('div');
    contactContainer.className = 'contact-container';
    contact = document.createElement('div');
    contact.className = 'contact-details';
    contact.addEventListener('click', function(e) {
	st = e.target.innerHTML;
	displayChat(st);
    });

    contactContainer.appendChild(contact);
    contact.innerHTML = keyid;
    contactList.appendChild(contactContainer);
}

function displayChat(id) {
    container = document.getElementById(id);
    myreceiver = id;
    container.style.display = 'inline-block';
    currentSelected && currentSelected != container && (currentSelected.style.display = 'none');
    currentSelected = container;
    currentSelected.scrollTop = currentSelected.scrollHeight;
}

function addMessages(messages, keyid) {
    addMessage(messages, keyid);
}

function handleResponse(x) {
    waiting = false;
}

function markMessagesDelivered(responseId) {
    if (responseId == null)
	return;
    body = {
	"_communicator": {
	    "_receiver": "postman@backend.com",
	    "_respid": responseId
	}	
    }
    sendRequest("requestResolver.py", "POST", JSON.stringify(body), handleResponse);
}

function sendRequest(url, method, requestBody, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
	callback(xhr.responseText);
    }
    xhr.open (method, url, true);
    xhr.send (requestBody);
    return false;
}


function getList(communications) {
    resp = {}
    communications.forEach(function callback(communication) {
	var arr = [];
	console.log("Condition ", !resp[communication._sendername], mysender, communication._sendername)
	if (!resp[communication._sendername]) {
	    sender = communication._sendername;
	    arr = communications.filter(function callback(comm) {
		return (comm._sendername == sender || comm._receivername == sender);
	    });
	    keyname = communication._sendername != mysender ? communication._sendername : communication._receivername;
	    resp[keyname] = arr;
	}
    });
    return resp;
}


function wrapUp(communications) {
    lst = getList(communications);
    for (var key in lst) {
	addMessages(lst[key], key);
    }
}

function handleMessages(response) {
    comm = JSON.parse(response);
    communications = comm._communications;
    if(communications.length > 0) {
	markMessagesDelivered(comm._responseid);
    }
    else
	waiting = false
    wrapUp(communications);
}

function getResponseMessages(data) {
    // reqid = JSON.parse(data)["_requestid"];
    body = {
	"_sender": mysender
    };
    sendRequest("responder.py", "POST", JSON.stringify(body), handleMessages);
}

function handleConversationsResponse(data) {
    waiting = false;
    displayApp();
}



function getConversationRequest(callback) {
    waiting = true;
    // callback = getResponseMessages;
    callback = handleConversationsResponse;
    sender = mysender
    receiver = "conversations@backend.com"
    url = "requestResolver.py";
    method = "POST";
    body = {
	"_communicator": {
	    "_receiver": receiver
	}
    }
    sendRequest(url, method, JSON.stringify(body), callback);
}

function sendMessage(ele) {
    if(event.which != 13) {
	return;
    }
    if (!myreceiver) {
	showToast("Please select a receiver first" , "error");
	return
    }
    input = document.getElementById('message');
    msg = input.value;
    input.value = "";
    message = { "_message" : msg }
    sender = mysender
    receiver = myreceiver;
    url = "requestResolver.py";
    method = "POST";
    body = {
	"_communicator": {
	    "_receiver": receiver,
	    "_message" : message
	}
    }
    addMessagePopUp(msg);
    sendRequest(url, method, JSON.stringify(body), handleResponse);
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    var mvalue;
    if (parts.length == 2)
	mvalue = parts.pop().split(";").shift();
    return mvalue.replace(/['"]+/g, '');
}

function displayApp() {
    loader = document.getElementById('loader');
    loader.style.display = 'none';
    app = document.getElementById('app-container');
    app.style.display = 'block';
}

function setSenderName() {
    clientid = getCookie('CID');
    var socket = new WebSocket("ws://localhost:9999/", clientid);
    socket.onopen = function(event) {
	console.log("Connection established");
	mysender = getCookie('userid');
	getConversationRequest()
    }

    socket.onclose = function(event) {
	console.log("Connection Failed");
    }
    socket.onmessage = function(e){
	var server_message = e.data;
	console.log(server_message);
	if (server_message == "PONG") {
	    checkForMessages();
	}
    }
   
    // setInterval(function() {
	// if(!waiting) {
	//     waiting = true;
	//     getResponseMessages();
	// }
    // }, 2000);
}

function checkForMessages() {
    if(!waiting) {
	waiting = true;
	getResponseMessages();
    }
}


function setReceiverName() {
    input = document.getElementById('receiver');
    name = input.value;
    myreceiver = name;
    showToast("Receiver: " + myreceiver, "success");
}


function blockContact() {
    input = document.getElementById('blockuser');
    blockname = input.value;
    if(!blockname) {
	showToast("Please select a receiver first" , "error");
	return;
    }
    url = "requestResolver.py";
    method = "POST";
    body = {
	"_communicator": {
	    "_receiver": "block@backend.com",
	    "_message" : blockname
	}
    }
    addMessagePopUp(msg);
    sendRequest(url, method, JSON.stringify(body), handleResponse);
}

function logout() {
    msgWindow = document.getElementById(messageWindowID);
    msgWindow.style.visibility = "hidden";
    document.cookie = "CID" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = "userid" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    location.reload();
}

window.onload = setSenderName();

