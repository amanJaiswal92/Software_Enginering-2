function showToast(message, toastClass, timeout) {
    timeout = timeout | 2000;
    container = document.createElement('div');
    container.className = "toast-container";
    toast = document.createElement('li');
    toast.className = "toast " + toastClass;
    toast.innerHTML = message;
    container.appendChild(toast);
    document.body.appendChild(container);
    setTimeout(function() {container.remove()}, timeout);
}
