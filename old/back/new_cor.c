#include<stdio.h>
#include<GL/glut.h>
#include<stdlib.h>

double rotate_y=0;
double rotate_x=0;

void display() {
  glClearColor(0.9,0.9,0.9,1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  glBegin(GL_QUADS);
  glColor3f(0.9,0.9,0.0);
  glVertex2i(-85,96);                         //  glVertex2f(-0.2,-0.2);glVertex2f(0.2,-0.2);glVertex2f(0.2,0.2);glVertex2f(-0.2,0.2);
  glVertex2i(85,96);
  glVertex2i(85,384);
  glVertex2i(-85,384);

  glColor3f(0.9,1.0,0.7);
  glVertex2i(-42,96); 
  glVertex2i(42,96);
  glVertex2i(42,300);
  glVertex2i(-42,300);
  
  glColor3f(0.9,0.9,0.7);
  glVertex2i(-85,384);
  glVertex2i(85,384);
  glVertex2i(683,768);
  glVertex2i(-683,768);

  glColor3f(0.8,0.8,0.8);
  glVertex2i(-85,96);
  glVertex2i(85,96);
  glVertex2i(683,-768);
  glVertex2i(-683,-768);
  glEnd();

  glFlush();
  
}

void Initialize(int x,int y) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-x, x, -y, y, -1.0, 1.0);
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  
  int w = glutGet(GLUT_SCREEN_WIDTH);
  int h = glutGet(GLUT_SCREEN_HEIGHT);
  //  printf("%d,%d\n",w,h);
  glutInitWindowSize(w,h);
  glutCreateWindow("triangle");

  glutInitWindowPosition(0,0);
  Initialize(w,h); 
  glutDisplayFunc(display);
  
  glutMainLoop(); //enter event processikng loop

  return 0;

}

