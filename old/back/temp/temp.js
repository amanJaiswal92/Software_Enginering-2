var room = {
    corner : {
	"cornerA" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerB" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
 		"color": "green"
	    }},
	"cornerC" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerD" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerE" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerF" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerG" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }},
	"cornerH" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "10",
		"color": "green"
	    }}
    },
    pillar : {
	"pillarAB" : {
	    dims : {
		"ab" : "10",
		"ad" : "40",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarAE" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "40",
		"color" : "blue"
	    }},
	"pillarAD" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarBC" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "40",
		"color" : "blue"
	    }},
	"pillarBF" : {
	    dims : {
		"ab" : "10",
		"ad" : "40",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarCD" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarCG" : {
	    dims : {
		"ab" : "10",
		"ad" : "10",
		"ae" : "40",
		"color" : "blue"
	    }},
	"pillarDH" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarEF" : {
	    dims : {
		"ab" : "10",
		"ad" : "40",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarEH" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarFG" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "10",
		"color" : "blue"
	    }},
	"pillarGH" : {
	    dims : {
		"ab" : "10",
		"ad" : "40",
		"ae" : "10",
		"color" : "blue"
	    }}
    },
    wall : {
	"wallABCD" : {
	    dims : {
		"ab" : "40",
		"ad" : "10",
		"ae" : "40",
		"color": "yellow"
	    }},
	"wallABFE" : {
	    dims : {
		"ab" : "40",
		"ad" : "40",
		"ae" : "10",
		"color": "yellow"
	    }},
	"wallADHE" : {
	    dims : {
		"ab" : "40",
		"ad" : "40",
		"ae" : "10",
		"color": "yellow"
	    }},
	"wallGHEF" : {
	    dims : {
		"ab" : "40",
		"ad" : "40",
		"ae" : "10",
		"color": "yellow"
	    }},
	"wallGCDH" : {
	    dims : {
		"ab" : "40",
		"ad" : "40",
		"ae" : "10",
		"color": "yellow"
	    }},
	"wallGCBF" : {
	    dims : {
		"ab" : "40",
		"ad" : "40",
		"ae" : "10",
		"color": "yellow"
	    }}
	
    }
}
