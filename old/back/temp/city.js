var geometry = new THREE.CubeGeometry( 1, 1, 1 );

geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 0.5, 0 ) );

geometry.faces.splice( 3, 1 );
geometry.faceVertexUvs[0].splice( 3, 1 );
