#include<stdio.h>
#include<GL/glut.h>
#include<stdlib.h>
#include<math.h>

void drawCircle(GLfloat x, GLfloat y, GLfloat z,GLfloat radius)
{
  int i;
  int triangleAmount = 100;
  GLfloat twicePi = 2.0f * 3.14;

  glEnable(GL_LINE_SMOOTH);
  glLineWidth(5.0);

  glBegin(GL_LINES);
  glColor4f(1.0, 1.0, 1.0, 1.0);
  for(i = 0; i <= triangleAmount; i++)
    {
      glVertex3f( x, y, z);
      glVertex3f(x + (radius * cos(i * twicePi / triangleAmount)), y + (radius * sin(i * twicePi / triangleAmount)), z);
    }
  glEnd();
}


void display2(){
  glClearColor(0,0,0,1.0);
  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_QUADS);
  glColor3f(0.0,0.0,0.0);
  glEnd();
  glFlush();
}

void display1(){
  glClearColor(0.9,0.9,0.9,1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  //    glBegin(GL_QUADS);    
  /*   glColor3f(1.0,1.0,1.0);                     // red color */
  /* glVertex3i(-85,96,-1);          */
  /* glVertex3i(85,96,-1); */
  /* glVertex3i(85,384,-1); */
  /* glVertex3i(-85,384,-1); */

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 0
  glVertex3i(-95,85,0);
  glVertex3i(-95,310,0);
  glVertex3i(-105,315,0);
  glVertex3i(-105,70,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 1
  glVertex3i(95,85,0);
  glVertex3i(95,310,0);
  glVertex3i(105,315,0);
  glVertex3i(105,70,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 2
  glVertex3i(-145,10,0);
  glVertex3i(-145,340,0);
  glVertex3i(-175,360,0);
  glVertex3i(-175,-30,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 3
  glVertex3i(-145,10,0);
  glVertex3i(-145,340,0);
  glVertex3i(-175,360,0);
  glVertex3i(-175,-30,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 4
  glVertex3i(145,10,0);
  glVertex3i(145,340,0);
  glVertex3i(175,360,0);
  glVertex3i(175,-30,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 5
  glVertex3i(-270,-200,0);
  glVertex3i(-270,400,0);
  glVertex3i(-320,435,0);
  glVertex3i(-320,-250,0);
  
  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 6
  glVertex3i(270,-200,0);
  glVertex3i(270,400,0);
  glVertex3i(320,435,0);
  glVertex3i(320,-250,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 7
  glVertex3i(-550,-600,0);
  glVertex3i(-550,530,0);
  glVertex3i(-750,630,0);
  glVertex3i(-750,-850,0);

  glBegin(GL_QUADS);
  glColor3f(0.3,0.1,0.0);                     // black color 8
  glVertex3i(550,-600,0);
  glVertex3i(550,530,0);
  glVertex3i(750,630,0);
  glVertex3i(750,-850,0);
  
  glColor3f(1.0,1.0,1.0);                     // black color 9
  glVertex3i(-42,200,1);
  glVertex3i(42,200,1);
  glVertex3i(42,300,1);
  glVertex3i(-42,300,1);

  glColor3f(0.8,0.8,0.8);                    // white color top
  glVertex3i(-85,384,1);
  glVertex3i(85,384,1);
  glVertex3i(600,768,1);
  glVertex3i(-600,768,1);

  glColor3f(1.0,1.0,1.0);                    // light 1
  glVertex3i(-70,400,1);
  glVertex3i(70,400,1);
  glVertex3i(85,410,1);
  glVertex3i(-85,410,1);

  glColor3f(1.0,1.0,1.0);                    // light 2
  glVertex3i(-160,500,1);
  glVertex3i(160,500,1);
  glVertex3i(190,520,1);
  glVertex3i(-190,520,1);

  glColor3f(1.0,1.0,1.0);                    // light 3
  glVertex3i(-300,650,1);
  glVertex3i(300,650,1);
  glVertex3i(370,700,1);
  glVertex3i(-370,700,1);

  glColor3f(0.8,0.8,0.8);                // floor
  glVertex3i(-85,96,1);
  glVertex3i(85,96,1);
  glVertex3i(683,-768,1);
  glVertex3i(-683,-768,1);
  glEnd();

  //drawCircle(30.0,180.0,1.0,2.0);                     // knob
  drawCircle(150.0,150.0,1.0,1.5);                     // knob
  drawCircle(-150.0,150.0,1.0,1.5);                     // knob
  drawCircle(280.0,80.0,1.0,5.0);                     // knob
  drawCircle(-280.0,80.0,1.0,5.0);                     // knob
  drawCircle(580.0,-100.0,1.0,10.5);                     // knob
  drawCircle(-580.0,-100.0,1.0,10.5);                     // knob
  
  glFlush();
  
}

void lightcontrol(unsigned char key, int x, int y)
{

  switch (key) {
  case 'w':
    glOrtho(-0.99, 0.99, -0.99, 0.99, -1.0, 1.0);
    glutDisplayFunc(display1); 
    glutPostRedisplay();
    break;
  case 's':
    glOrtho(1.01, -1.01, -1.01, 1.01, -1.0, 1.0);
    glutDisplayFunc(display1);
    glutPostRedisplay();
    break;
  }
}

void Initialize(int x,int y) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-x, x, -y, y, -1.0, 1.0);
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  
  int w = glutGet(GLUT_SCREEN_WIDTH);
  int h = glutGet(GLUT_SCREEN_HEIGHT);
  //  printf("%d,%d\n",w,h);
  glutInitWindowSize(w,h);
  glutCreateWindow("CORRIDOR");
  
  glutInitWindowPosition(0,0);
  Initialize(w,h);
  glutKeyboardFunc(lightcontrol);
  glutDisplayFunc(display1);
  
  glutMainLoop(); //enter event processikng loop

  return 0;

}
