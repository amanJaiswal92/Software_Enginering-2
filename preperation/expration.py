#remove unwanted parentheses 
def validateExpression(exp):
    r = list(exp)
    s = list(exp)
    st = []
    i = 0
    while(i<len(s)):
        if s[i]=='(':
            if s[i+1] == '(':
                st.append(-i)
            else:
                st.append(i)
            i +=1
        elif s[i]!=')' and s[i]!='(':
            i +=1
        elif s[i] == ')':
            top = st[-1]
            if s[i-1]==')' and top<0:
                r[-top] = '$'
                r[i] = '$'
                st.pop()
            elif s[i-1]==')' and top > 0:
                print("Something went wrong")
            elif s[i-1] != ")" and top > 0:
                st.pop()
            i +=1
    st = ""
    for i in range(len(r)):
        if r[i]=='$':
            continue
        st +=r[i]
    return st


def main():
    exp = input()
    print(validateExpression(exp))

if __name__=="__main__":
    main()
