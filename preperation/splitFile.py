import sys

def main(argv):
    fp = open(argv[1],'r')
    line = int(argv[2])
    width = int(argv[3])
    #print(fp,line,width)
    data = fp.read()
    fp = open('output.txt','w')
    count = 0
    lineCount = 0
    ls = ""
    pageNumber = 0
    for i in data.split('\n'):
        for j in i.split(' '):
            if count == width:
                lineCount +=1
                count = 0
                fp.write(ls+'\n')
                ls = ""
            if lineCount == line:
                lineCount = 0
                sp = '\n'+str(pageNumber) +"  " +"-"*20+'\n'
                pageNumber +=1
                fp.write(sp)

            if count+len(j)<=width:
                ls +=j+" "
                count += len(j)+1
            else:
                count = width

    fp.close()



if __name__=="__main__":
    main(sys.argv)
