AMAN JAISWAL 14125 MCA(3rd Year) CD, Compact Disc, 
a quite popular storage media. It is basically an 
optical disc generally having a capacity of data 

0  --------------------
equivalent to 700mb. While CDs are played they 
around and a laser beam is used to read the data. 
To store data on a CD, they need to be burned.  A 

1  --------------------
CD is made from 1.2 mm thick, almost-pure 
plastic (known as Polymethyle Meta acrylic) and 
15–20 grams. The surface of a CD is made of a 

2  --------------------
layer with molded spiral tracks on the top. The 
of the CD is coated with a thin layer of aluminium 
make it reflective, and is protected by a film of 

3  --------------------
lacquer that is normally spin coated directly on 
of the reflective layer, upon which the label 
is applied.   From the center outward components 

4  --------------------
at the center (spindle) hole, the first-transition 
(clamping ring), the clamping area (stacking 
the second-transition area (mirror band), the 

5  --------------------
(data) area, and the rim. Standard CDs have a 
of 120 mm and can hold up to 80 minutes of 
audio (700 MB of data). The Mini CD has various 

6  --------------------
ranging from 60 to 80 mm; they are sometimes used 
for CD singles or device drivers, storing up to 24 
of audio.   DATA STORAGE: The data are stored on 

7  --------------------
CD as a series of minute grooves which are known 
‘pits’ encoded on these spiral tracks. The areas 
the ‘pits’ are known as ‘lands’. Each pit is 

8  --------------------
100 nm deep by 500 nm wide, and varies from 850 nm 
3.5 µm in length. These pits and lands do not 
the 1s and 0s, rather each change from pit to land 

9  --------------------
land to pit is interpreted as 0 while no change is 
as 1.    BURNING PROCESS: The burning process of a 
is nothing but creating a pattern of pits and 

10  --------------------
over the polycarbonate layer. But since the data 
be accurately encoded on such a small scale, the 
process must be extremely précised. A CD burner is 

11  --------------------
to write (burn) the data on a CD. It incorporates 
a moving laser quite similar to a CD player which 
is known as ‘Write Laser’. The Write Laser which 

12  --------------------
more powerful than the ‘Read Laser’, has the 
to alter the surface of CD instead of just 
the laser light off. During burning process, as 

13  --------------------
the data (binary values) the Write Laser bounces 
light beam over the CD surface and creates a 
of pits on it.  RETRIVE DATA:  When you play the 

14  --------------------
the Read Laser bounces the light beams (not 
to modify the surface of CD) on the surface and 
the pits and lands. Each change between pit to 

15  --------------------
or vice versa is translated as zero and no change 
(pit to pit or land to land) is translated as one. 
binary values form the actual data.  About 20000 

16  --------------------
more tracks are found in a CD’s recording surface. 
distance between the tracks, the pitch, is 1.6 µm. 
CD is read by focusing a 780 nm wavelength (near 

17  --------------------
semiconductor laser through the bottom of the 
layer. The change in height between pits and lands 
in a difference in intensity in the light 

18  --------------------
By measuring the intensity change with a 
the data can be read from the disc. The digital 
is defined as the length of pits and distance 

19  --------------------
them. The pits and reflective surface represents 
0 and logic 1. The pits and lands themselves do 
directly represent the zeros and ones of binary 

20  --------------------
Instead, Non-return-to-zero, inverted (NRZI) 
is used: a change from pit to land or land to pit 
indicates a one, while no change indicates a 

21  --------------------
of zeros. There must be at least two and no more 
ten zeros between each one, which is defined by 
length of the pit.  The laser diode- lens assembly 

22  --------------------
the optical system of the CD player. The laser 
lens assembly is generally known as ‘Eye of CD 
The lens system focuses the laser beam reflected 

23  --------------------
the CD and reflected back light is collected by 
objective lens and transmitted to the detector 
When a Laser beam is focused on to the CD, because 

24  --------------------
a difference between the depth of pits and 
of the laser beam, a phase difference develops 
the light reflected from pits and the reflecting 

25  --------------------
The reflected light is then modulated by the 
system. Before passing to the detector, the 
laser beam is polarized and aligned to 90 degrees. 

26  --------------------
detector is a photo sensor that produces 
electrical signals which are then amplified and 
into corresponding video and audio signals.  CD 

27  --------------------
• Scanning velocity: 1.2–1.4 m/s (constant linear 
velocity) – equivalent to approximately 500 rpm at 
inside of the disc, and approximately 200 rpm at 

28  --------------------
outside edge. (A disc played from beginning to end 
down during playback.) • Track pitch: 1.6 µm • 
diameter 120 mm • Disc thickness: 1.2 mm • Inner 

29  --------------------
program area: 25 mm • Outer radius program area: 
mm • Center spindle hole diameter: 15 mm • 
between tracks in microns: 1,6 INFO: DVDs are of 

30  --------------------
same diameter and thickness as CDs, and they are 
using some of the same materials and manufacturing 
Like a CD, the data on a DVD is encoded in the 

31  --------------------
of small pits and bumps in the track of the disc. 
 A DVD is composed of several layers of plastic, 
about 1.2 millimeters thick. Each layer is created 

32  --------------------
injection molding polycarbonate plastic. This 
forms a disc that has microscopic bumps arranged 
a single, continuous and extremely long spiral 

33  --------------------
of data. More on the bumps later.  How do DVDs 
more than CDs? The answer is simple. If you've 
had to squeeze a certain amount of text on a 

34  --------------------
sheet of paper (maybe to make a poster) and found 
it difficult to get everything on, you'll know 
a simple solution: you just make your words a bit 

35  --------------------
smaller (lower the font size). The same idea works 
you're writing computer data on discs with laser 
You can store more on a DVD than a CD by using a 

36  --------------------
beam that "writes smaller". And to read or write a 
disc, you use a laser to write even smaller still. 
A DVD uses a red laser beam that makes light waves 

37  --------------------
a wavelength of 650 nanometers (0.00000065 meters, 
less than one hundredth the width of a human 
That's considerably shorter than the wavelength of 

38  --------------------
infrared light that a CD player uses (780 
which is why DVDs can store more than CDs. A 
player uses an even more precise laser than a DVD 

39  --------------------
player, with a beam of blue light shooting out of 
it instead of red or infared. Blue light has a 
shorter wavelength (about 450 nanometers) than red 

40  --------------------
so a blue laser can write things that are far 
That means Blu-ray discs can store movies in a 
higher quality format known as High Definition 

41  --------------------
store much longer movies on a single disc, or just 
more altogether. If you can fit four, half-hour 
of Friends on a DVD, you can fit 24 episodes (a 

42  --------------------
series) on a Blu-ray disc.  DVD PARAMETERS: • 
velocity: • Track pitch: 0.4 µm • Disc diameter 
mm • Disc thickness: 1.2 mm • Center spindle hole 

43  --------------------
diameter: 15 mm • Distance between tracks in 
0,74  Parameter                          DVD-R    
       CD-R Media Type                      

44  --------------------
     Write-once Wavelength (Recording)          
– 645 nm    775 – 795 nm Wavelength (Reading)     
       635 – 650 nm    770 – 830 nm Recording 

45  --------------------
                6-12 mw         4 – 8 mw Numerical 
(Recording)  0.60            0.50 Numerical 
