
ls = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
opList = ['/','*','+','-']

def infixToPostfix(exp):
    s = []
    prec = {}
    prec['/'] = 3
    prec['*'] = 3
    prec['+'] = 2
    prec['-'] = 2
    prec['('] = 1
    token = list(exp)
    out = []
    for t in token:
        if t in ls:
            out.append(t)
        elif t == '(':
            s.append(t)
        elif t == ')':
            topToken = s.pop()
            while topToken !='(':
                out.append(topToken)
                topToken = s.pop()
        else:
            while (not len(s)==0) and (prec[s[-1]]>=prec[t]):
                out.append(s.pop())
            s.append(t)
            print(s[-1])
    while not len(s)==0:
        optoken = s.pop()
        out.append(optoken)
    return "".join(out)

def postfixToInfic(exp):
    s = []
    for i in exp:
        if i in '+-':
            right = s.pop()
            left = s.pop()
            newExp = left+i+right
            s.append((newExp,i))
        elif i in '*/':
            right = s.pop()
            if right[1]=='+' or right[1]=='-':
                right = "(" + right[0] + ")"
            else:
                right = right[0]
            left = s.pop()
            if left[1]=='+' or left[1]=='-':
                left = "("+left[0]+")"
            else:
                left = left[0]
            newExp = left + i + right
            s.append((newExp,i))
        else:
            s.append((i,""))
    return s
'''
    for i in exp:
        if i in ls:
            s.append(i)
        elif i in opList:
            op1 = s.pop()
            op2 = s.pop()
            ll = op1 + i + op2
            s.append(ll)
    return "".join(s)
'''
def main():
    exp = input()
    postfix = infixToPostfix(exp)
    infix = postfixToInfic(postfix)
    print(infix)

if __name__=="__main__":
    main()

